package org.intely.core.resources;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.intely.core.domain.Activity;
import org.intely.core.domain.Deposit;
import org.intely.core.domain.Wallet;
import org.intely.core.dto.DepositDTO;
import org.intely.core.dto.DebitDTO;
import org.intely.core.exception.DebitedWalletNotFound;
import org.intely.core.exception.InsufficientFundsException;
import org.intely.core.exception.InvalidFieldsException;
import org.intely.core.exception.WalletAlreadyExists;
import org.intely.core.helpers.Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.intely.core.repositories.DepositRepository;
import org.intely.core.repositories.WalletRepository;
import org.intely.core.services.DepositService;

@RestController
@RequestMapping("/server/v1/deposits")
public class DepositResource {

    @Autowired
    private DepositRepository depositRepository;
    @Autowired
    private DepositService depositService;
    @Autowired
    private WalletRepository walletRepository;

    @GetMapping("/{id}")
    public Deposit get(@PathVariable Long id) {
        return depositRepository.findById(id).get();
    }

    @GetMapping
    public List<Deposit> get() {
        return depositRepository.findAll();
    }

    @PostMapping
    public void add(@RequestBody DepositDTO depositDTO) throws InvalidFieldsException, IOException, WalletAlreadyExists, DebitedWalletNotFound, InsufficientFundsException {
        depositDTO.validate();
        ArrayList<Wallet> debitedWallets = new ArrayList();
        for (DebitDTO te : depositDTO.getInputs()) {
            Wallet debitedWallet = walletRepository.findWalletByPublicKeyHash(Hash.sha256(te.getPublicKey()));
            if (debitedWallet != null) {
                if (debitedWallet.getBalance().compareTo(new BigDecimal(te.getTxValue())) == -1) {
                    throw new InsufficientFundsException();
                } else {
                    debitedWallets.add(debitedWallet);
                }
            } else {
                throw new DebitedWalletNotFound();
            }
        }
        Wallet creditedWallet = walletRepository.findWalletByPublicKeyHash(depositDTO.getCreditedPublicKeyHash());
        if (creditedWallet != null) {
            throw new WalletAlreadyExists();
        }
        Deposit deposit = depositDTO.toDeposit();
        Wallet newWallet = new Wallet(deposit);
        newWallet.setDeposit(deposit);
        deposit.setCreditedWallet(newWallet);
        //deposit.setActivity(new Activity(deposit));
        deposit.getInputs().forEach((input) -> {
            for (Wallet debitedWallet : debitedWallets) {
                if (Arrays.equals(debitedWallet.getPublicKeyHash(), Hash.sha256(input.getPublicKey()))) {
                    input.setDebitedWallet(debitedWallet);
                    input.setDeposit(deposit);
                    //input.setActivity(new Activity(input, deposit));
                }
            }
        });
        depositService.publishDeposit(deposit);
    }
}
