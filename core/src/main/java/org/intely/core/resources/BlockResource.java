package org.intely.core.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.intely.core.configs.SetupConfigs;
import org.intely.core.domain.Block;
import org.intely.core.dto.BlockDTO;
import org.intely.core.exception.InvalidFieldsException;
import org.intely.core.repositories.BlockRepository;
import org.intely.core.services.BlockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/server/v1/blocks")
public class BlockResource {

    @Autowired
    private BlockRepository repository;
    @Autowired
    private BlockService service;

    @GetMapping("/{height}")
    public List<Block> get(@PathVariable Long height) {
        return repository.findBlocksByHeight(height);
    }

    @GetMapping("/byHash/{hashHex}/{size}")
    public List<BlockDTO> getByHash(@PathVariable String hashHex, Integer size) throws DecoderException {
        if (size == null) {
            size = 10;
        } else if (size > 10) {
            size = 10;
        } else if (size < 1) {
            size = 1;
        }
        byte[] hash = Hex.decodeHex(hashHex);
        ArrayList<BlockDTO> blocks = new ArrayList();
        for (int i = 0; i < size; i++) {
            Block block = repository.findBlockByHash(hash);
            if (block != null && block.getHeight() != 0) {
                hash = block.getPreviousBlockHash();
                blocks.add(new BlockDTO(block));
            } else {
                break;
            }
        }
        return blocks;
    }

    @GetMapping
    public List<Block> get() {
        return repository.findAll();
    }

    @PostMapping("/add")
    public void add(@RequestBody BlockDTO blockDTO, HttpServletRequest request) throws InvalidFieldsException {
        blockDTO.validate();
        Block block = blockDTO.toBlock();
        if (block.getHeight() != null && block.getHeight() < service.getHighestBlock() - SetupConfigs.getInstance().getPruningLimit()) {
            return;
        }
        Block myBlock = repository.findBlockByHash(block.getBlockHash());
        if (myBlock == null) {
            service.publishBlock(block);
        }
    }
}
