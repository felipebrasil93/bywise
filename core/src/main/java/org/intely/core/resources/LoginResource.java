package org.intely.core.resources;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.intely.core.configs.SetupConfigs;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class LoginResource {
    
    @GetMapping("/")
    public ResponseEntity<String> index() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return new ResponseEntity<>("Running: "+SetupConfigs.SOFTWARE+" - "+sdf.format(new Date()), HttpStatus.OK);
    }
    
    @GetMapping("/core/debug")
    public ResponseEntity<String> debug() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return new ResponseEntity<>("Debug: "+SetupConfigs.SOFTWARE+" - "+sdf.format(new Date()), HttpStatus.OK);
    }
    
    @GetMapping("/core/debug2")
    public ResponseEntity<String> debug2() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return new ResponseEntity<>("Debug 2: "+SetupConfigs.SOFTWARE+" - "+sdf.format(new Date()), HttpStatus.OK);
    }
}
