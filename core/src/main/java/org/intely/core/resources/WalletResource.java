package org.intely.core.resources;

import java.util.ArrayList;
import java.util.List;
import org.intely.core.domain.Wallet;
import org.intely.core.dto.BalanceDTO;
import org.intely.core.repositories.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/server/v1/wallets")
public class WalletResource {

    @Autowired
    private WalletRepository walletRepository;

    @PostMapping
    public ArrayList<BalanceDTO> sync(@RequestBody List<byte[]> hashs) {
        ArrayList<BalanceDTO> balances = new ArrayList();
        for (int i = 0; i < hashs.size(); i++) {
            Wallet w = walletRepository.findWalletByPublicKeyHash(hashs.get(i));
            if(w!=null){
                balances.add(new BalanceDTO(w.getPublicKeyHash(), w.getBalance(), w.getLastOperation()));
            }
        }
        return balances;
    }
}
