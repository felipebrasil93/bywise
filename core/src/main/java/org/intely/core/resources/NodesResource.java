package org.intely.core.resources;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.intely.core.configs.SetupConfigs;
import org.intely.core.domain.Node;
import org.intely.core.domain.enums.NodeTypeEnum;
import org.intely.core.dto.*;
import org.intely.core.exception.CantConnectException;
import org.intely.core.exception.DisconectedNodeException;
import org.intely.core.exception.InvalidFieldsException;
import org.intely.core.exception.WalletNodeException;
import org.intely.core.services.BlockService;
import org.intely.core.services.NodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/server/v1/nodes")
public class NodesResource {

    @Autowired
    private NodeService nodeService;
    @Autowired
    private BlockService blockService;

    @GetMapping("/info")
    public InfoDTO look(HttpServletRequest request) {
        InfoDTO info = nodeService.generateMyInfoDTO();
        info.setYouAddress(request.getRemoteAddr());
        info.setHeight(blockService.getHighestBlock());
        return info;
    }

    @GetMapping("/ping/{port}")
    public void ping(@PathVariable Integer port, HttpServletRequest request) throws DisconectedNodeException, WalletNodeException {
        nodeService.consultNodeInfo(new Node(SetupConfigs.getInstance().getNetwork(), request.getRemoteAddr(), port, "ping-test"));
    }

    @PostMapping("/handshake")
    public HandshakeTokenDTO handshake(@RequestBody HandshakeDTO handshakeDTO, HttpServletRequest request) {
        try {
            handshakeDTO.validate();
        } catch (InvalidFieldsException ex) {
            return new HandshakeTokenDTO("Missing parameters " + ex.getMessage());
        }
        Node n = new Node(request.getRemoteAddr(), handshakeDTO);
        try {
            String token = nodeService.forceConnectNode(n);
            nodeService.validateNodeConnectionWithToken(n);
            HandshakeTokenDTO myHand = new HandshakeTokenDTO();
            myHand.setToken(token);
            myHand.setMinDelay(SetupConfigs.getInstance().getMinDelay());
            myHand.setName(SetupConfigs.getInstance().getName());
            myHand.setSoftware(SetupConfigs.SOFTWARE);
            myHand.setVersion(SetupConfigs.VERSION);
            myHand.setHeight(blockService.getHighestBlock());
            myHand.setType(NodeTypeEnum.MINNER);
            return myHand;
        } catch (CantConnectException | WalletNodeException ex) {
            nodeService.forceDisconnectNode(n);
            return new HandshakeTokenDTO(ex.getMessage());
        } catch (DisconectedNodeException ex) {
            nodeService.forceDisconnectNode(n);
            return new HandshakeTokenDTO("Unable to validate uploaded token -> " + ex.getMessage());
        }
    }

    @PostMapping("/wallet/handshake")
    public HandshakeTokenDTO walletHandshake(@RequestBody WalletHandshakeDTO handshakeDTO, HttpServletRequest request) {
        try {
            handshakeDTO.validate();
        } catch (InvalidFieldsException ex) {
            return new HandshakeTokenDTO("Missing parameters " + ex.getMessage());
        }
        Node n = new Node(request.getRemoteAddr(), handshakeDTO);
        try {
            String token = nodeService.forceConnectNode(n);
            HandshakeTokenDTO myHand = new HandshakeTokenDTO();
            myHand.setToken(token);
            myHand.setMinDelay(SetupConfigs.getInstance().getMinDelay());
            myHand.setName(SetupConfigs.getInstance().getName());
            myHand.setSoftware(SetupConfigs.SOFTWARE);
            myHand.setVersion(SetupConfigs.VERSION);
            myHand.setHeight(blockService.getHighestBlock());
            myHand.setType(NodeTypeEnum.MINNER);
            return myHand;
        } catch (CantConnectException ex) {
            nodeService.forceDisconnectNode(n);
            return new HandshakeTokenDTO(ex.getMessage());
        }
    }

    @GetMapping("/validate")
    public void validate(HttpServletRequest request) {
    }
}
