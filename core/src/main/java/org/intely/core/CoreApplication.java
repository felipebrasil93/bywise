package org.intely.core;

import org.intely.core.configs.SetupConfigs;
import org.intely.core.components.Wallets;
import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStyle;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.shell.jline.PromptProvider;

@EnableAsync
@EnableScheduling
@SpringBootApplication
public class CoreApplication {

    public static void main(String[] args) {
        System.getProperties().put("server.port", SetupConfigs.getInstance().getPort());
        Wallets.getInstance();
        ConfigurableApplicationContext ctx = SpringApplication.run(CoreApplication.class, args);
    }

    @Bean
    public PromptProvider myPromptProvider() {
        return () -> new AttributedString("intely:>", AttributedStyle.DEFAULT.foreground(AttributedStyle.YELLOW));
    }
    
}
