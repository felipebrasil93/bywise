package org.intely.core.security;

import java.io.IOException;
import java.util.Date;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.intely.core.exception.RateLimitException;
import org.intely.core.services.NodeDetailsService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private final JWTUtil jwtUtil;
    private final NodeDetailsService userDetailsService;

    public JWTAuthorizationFilter(JWTUtil jwtUtil, NodeDetailsService userDetailsService, AuthenticationManager authenticationManager) {
        super(authenticationManager);
        this.jwtUtil = jwtUtil;
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String token = request.getHeader("Authorization");
        if (token != null) {
            try {
                UsernamePasswordAuthenticationToken auth = getAuthentication(token);
                if (auth != null) {
                    SecurityContextHolder.getContext().setAuthentication(auth);
                }
            } catch (RateLimitException ex) {
            }
        }
        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(String token) throws RateLimitException {
        if (jwtUtil.isValid(token)) {
            String user = jwtUtil.getUser(token);
            NodeSS nodeSS = (NodeSS) userDetailsService.loadUserByUsername(user);
            nodeSS.getNode().receiverRequest();
            return new UsernamePasswordAuthenticationToken(nodeSS, null, nodeSS.getAuthorities());
        }
        return null;
    }

}
