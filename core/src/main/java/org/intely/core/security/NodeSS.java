package org.intely.core.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.intely.core.domain.Node;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class NodeSS implements UserDetails {
    private static final long serialVersionUID = 1L;

    private final String id;
    private final Node node;
    private final List<GrantedAuthority> authorities;

    public NodeSS(Node node) {
        this.id = node.getId();
        this.node = node;
        this.authorities = new ArrayList();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return "";
    }

    @Override
    public String getUsername() {
        return id;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public Node getNode() {
        return node;
    }
}
