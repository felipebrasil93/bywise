package org.intely.core.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.intely.core.configs.P2PConfigs;
import org.intely.core.configs.SetupConfigs;
import org.intely.core.domain.Node;
import org.intely.core.services.NodeDetailsService;
import org.springframework.stereotype.Component;

@Component
public class JWTUtil {

    public Node getNode(HttpServletRequest request, NodeDetailsService nodeDetailsService) {
        String token = request.getHeader("Auth");
        String user = getUser(token);
        NodeSS nodeSS = (NodeSS) nodeDetailsService.loadUserByUsername(user);
        return nodeSS.getNode();
    }

    public String generateToken(Node node) {
        node.setExpiration(System.currentTimeMillis() + P2PConfigs.TOKEN_EXPIRATION);
        return Jwts.builder()
                .setSubject(node.getId())
                .setExpiration(new Date(node.getExpiration()))
                .signWith(SignatureAlgorithm.HS256, SetupConfigs.getInstance().getSecretJWT())
                .compact();
    }

    String getUser(String token) {
        Claims claims = getClaims(token);
        return claims.getSubject();
    }

    boolean isValid(String token) {
        Claims claims = getClaims(token);
        if (claims != null) {
            String user = claims.getSubject();
            Date expirationDate = claims.getExpiration();
            Date now = new Date(System.currentTimeMillis());
            if (user != null && expirationDate != null && now.before(expirationDate)) {
                return true;
            }
        }
        return false;
    }

    private Claims getClaims(String token) {
        try {
            return Jwts.parser().setSigningKey(SetupConfigs.getInstance().getSecretJWT()).parseClaimsJws(token).getBody();
        } catch (Exception ex) {
            return null;
        }
    }

    public static Long getExpiration(String token) {
        try {
            int i = token.lastIndexOf('.');
            String tokenWithoutSignature = token.substring(0, i + 1);
            Jwt<Header,Claims> untrustedJWT = Jwts.parser().parseClaimsJwt(tokenWithoutSignature);
            return untrustedJWT.getBody().getExpiration().getTime();
        } catch (Exception ex) {
            return null;
        }
    }
}
