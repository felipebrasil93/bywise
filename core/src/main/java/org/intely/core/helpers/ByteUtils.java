package org.intely.core.helpers;

import java.nio.ByteBuffer;

public class ByteUtils {
    private static ByteBuffer BUFFER = ByteBuffer.allocate(Long.BYTES);    

    public static byte[] longToBytes(long x) {
        BUFFER.putLong(0, x);
        return BUFFER.array();
    }

    public static long bytesToLong(byte[] bytes) {
        BUFFER.put(bytes, 0, bytes.length);
        BUFFER.flip();
        return BUFFER.getLong();
    }
}
