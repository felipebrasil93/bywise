package org.intely.core.helpers;

import fr.cryptohash.*;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Hash {
    
    public static byte[] X12(byte[] hash){
        Groestl512 groestl = new Groestl512();
        hash = groestl.digest(hash);
        
        BLAKE512 blake512 = new BLAKE512();
        hash = blake512.digest(hash);

        Skein512 skein = new Skein512();
        hash = skein.digest(hash);
        
        BMW512 bmw = new BMW512();
        hash = bmw.digest(hash);

        Keccak512 keccak = new Keccak512();
        hash = keccak.digest(hash);
        
        JH512 jh = new JH512();
        hash = jh.digest(hash);
        
        SIMD512 simd = new SIMD512();
        hash = simd.digest(hash);
        
        Luffa512 luffa = new Luffa512();
        hash = luffa.digest(hash);

        SHAvite512 shavite = new SHAvite512();
        hash = shavite.digest(hash);
        
        CubeHash512 cubehash = new CubeHash512();
        hash = cubehash.digest(hash);

        ECHO512 echo = new ECHO512();
        hash = echo.digest(hash);
        
        SHA256 sha256 = new SHA256();
        hash = sha256.digest(hash);
        hash[0] = (byte) (hash[0] & 0b01111111);
        return hash;
    }

    public static byte[] sha256(byte[] data) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(data);
            return hash;
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Hash.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(-1);
        }
        return null;
    }
    
    public static byte[] sha512(byte[] data) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-512");
            byte[] hash = digest.digest(data);
            return hash;
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Hash.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(-1);
        }
        return null;
    }
    
    public static String sha256String(byte[] data) {
        return Base64.getEncoder().encodeToString(data);
    }
    
    public static byte[] sha256(String data) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            return digest.digest(data.getBytes("ASCII"));
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            Logger.getLogger(Hash.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(-1);
        }
        return null;
    }
}
