package org.intely.core.helpers;

import java.io.File;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.intely.core.configs.SetupConfigs;
import org.intely.core.domain.MyWallet;
import org.intely.core.exception.InvalidAddress;
import org.json.JSONArray;

public class WalletTools {

    private static final int KEY_PAIR_SIZE = 1024;
    private static final int HASH_SIZE = 10;
    private static final String INIT_ADDRESS_STR = "inte1yAA";
    private static final byte[] INIT_ADDRESS = new byte[]{4, 115, 68, -52, 60, 81};

    public static void main(String[] args) throws Exception {
        String hash = Base64.getEncoder().encodeToString(Hash.sha512("sdfsdfsdf".getBytes()));
        JSONArray arr = new JSONArray();
        for (int i = 0; i < 65536; i++) {
            arr.put(hash);
        }
        HelperFile.saveFile(new File("test.json"), arr.toString());
        long i = 65536;
        System.out.println(i*i);
    }
    
    public static String getAddress(byte[] publicKey) {
        byte[] preAddress = new byte[INIT_ADDRESS.length + 1 + publicKey.length];
        System.arraycopy(INIT_ADDRESS, 0, preAddress, 0, INIT_ADDRESS.length);
        preAddress[INIT_ADDRESS.length] = SetupConfigs.getInstance().getNetwork().getWalletAddress();
        System.arraycopy(publicKey, 0, preAddress, INIT_ADDRESS.length + 1, publicKey.length);
        byte[] address = new byte[preAddress.length + HASH_SIZE];
        byte[] hash = Hash.sha256(preAddress);
        System.arraycopy(preAddress, 0, address, 0, preAddress.length);
        System.arraycopy(hash, 0, address, preAddress.length, HASH_SIZE);
        return Base58.encode(address);
    }

    public static byte[] getPublicKey(String address) throws InvalidAddress {
        if (address.startsWith(INIT_ADDRESS_STR) && address.length() == 247) {
            byte[] addressBytes = Base58.decode(address);
            byte[] preAddress = new byte[addressBytes.length - HASH_SIZE];
            byte[] hash = new byte[HASH_SIZE];
            System.arraycopy(addressBytes, 0, preAddress, 0, preAddress.length);
            System.arraycopy(addressBytes, preAddress.length, hash, 0, hash.length);
            byte[] calcHash = Hash.sha256(preAddress);
            boolean equalsHash = true;
            for (int i = 0; i < HASH_SIZE && equalsHash; i++) {
                equalsHash = calcHash[i] == hash[i];
            }
            if (equalsHash) {
                if (addressBytes[INIT_ADDRESS.length] == SetupConfigs.getInstance().getNetwork().getWalletAddress()) {
                    byte[] publicKey = new byte[addressBytes.length - HASH_SIZE - INIT_ADDRESS.length - 1];
                    System.arraycopy(addressBytes, INIT_ADDRESS.length + 1, publicKey, 0, publicKey.length);
                    return publicKey;
                } else {
                    throw new InvalidAddress("address does not work in " + SetupConfigs.getInstance().getNetwork());
                }
            } else {
                throw new InvalidAddress("Corrupted address");
            }
        } else {
            throw new InvalidAddress("Address does not start with '" + INIT_ADDRESS_STR + "'");
        }
    }

    public static byte[] sign(MyWallet myWallet, byte[] data) {
        try {
            return sign(data, myWallet.getPrivateKey());
        } catch (Exception ex) {
            Logger.getLogger(WalletTools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static boolean verify(MyWallet myWallet, byte[] data, String signature) {
        try {
            byte[] signatureBytes = Base64.getDecoder().decode(signature);
            return verify(data, signatureBytes, myWallet.getPublicKey());
        } catch (Exception ex) {
            Logger.getLogger(WalletTools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static MyWallet generateWallet() {
        try {
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
            keyGen.initialize(KEY_PAIR_SIZE, new SecureRandom());
            KeyPair pair = keyGen.generateKeyPair();

            X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(pair.getPublic().getEncoded());
            PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(pair.getPrivate().getEncoded());

            byte[] publicKey = x509EncodedKeySpec.getEncoded();
            byte[] privateKey = pkcs8EncodedKeySpec.getEncoded();

            return new MyWallet(publicKey, privateKey);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(WalletTools.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private static byte[] sign(byte[] data, byte[] private_key) throws Exception {
        Signature privateSignature = Signature.getInstance("SHA256withRSA");
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(private_key);
        PrivateKey privateKeyDecoded = keyFactory.generatePrivate(privateKeySpec);
        privateSignature.initSign(privateKeyDecoded);
        privateSignature.update(data);
        return privateSignature.sign();
    }

    public static boolean verify(byte[] data, byte[] signature, byte[] public_key) throws Exception {
        Signature publicSignature = Signature.getInstance("SHA256withRSA");
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec public_keySpec = new X509EncodedKeySpec(public_key);
        PublicKey public_keyDecoded = keyFactory.generatePublic(public_keySpec);
        publicSignature.initVerify(public_keyDecoded);
        publicSignature.update(data);
        return publicSignature.verify(signature);
    }
}
