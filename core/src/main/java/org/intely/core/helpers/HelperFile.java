package org.intely.core.helpers;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HelperFile {

    public static void saveFile(File file, String s) {
        FileWriter fw = null;
        try {
            fw = new FileWriter(file);
            PrintWriter pw = new PrintWriter(fw);
            pw.print(s);
            fw.close();
        } catch (IOException ex) {
            Logger.getLogger(HelperFile.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fw.close();
            } catch (IOException ex) {
                Logger.getLogger(HelperFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static String readFile(File file, boolean excludeCommentedLines) {
        try {
            StringBuilder sb = new StringBuilder();
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            while (br.ready()) {
                String line = br.readLine().trim();
                if (!line.startsWith("#") || !excludeCommentedLines) {
                    sb.append(line);
                    sb.append("\n");
                }
            }
            br.close();
            fr.close();
            return sb.toString();
        } catch (Exception ex) {
            new RuntimeException(ex);
        }
        return null;
    }
}
