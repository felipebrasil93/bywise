package org.intely.core.components;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.intely.core.domain.Block;
import org.intely.core.domain.Wallet;
import org.intely.core.helpers.Hash;
import org.intely.core.repositories.BlockRepository;
import org.intely.core.repositories.WalletRepository;
import org.intely.core.services.BlockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@EnableScheduling
@Component
public class BlockchainMainThread implements Runnable {

    private static boolean needToRunStartupMethod = true;

    @Autowired
    private BlockRepository blockRepository;
    @Autowired
    private WalletRepository walletRepository;
    @Autowired
    private BlockService blockService;

    @Scheduled(fixedRate = 3600000)
    public void startBlockchainMain() {
        if (needToRunStartupMethod) {
            needToRunStartupMethod = false;
            Thread thread = new Thread(this);
            thread.setName(this.getClass().getSimpleName());
            thread.start();
        }
    }

    @Override
    public void run() {
        while (true) {
            Block block = blockService.getHighestBlockNotMain();
            if (block != null && block.getHeight() > blockService.getHighestBlock()) {
                ArrayList<Block> branch = new ArrayList();
                do {
                    branch.add(block);
                    block = blockRepository.findBlockByHash(block.getPreviousBlockHash());
                } while (block != null && !block.getIsMain() && block.getIsValidated());
                if (block != null && block.getIsValidated()) {
                    for (int i = 0; i < branch.size(); i++) {
                        block = branch.get(i);
                        if (!block.getIsMain()) {
                            Block executedBlock = blockRepository.findMainBlockByHeight(block.getHeight());
                            if (executedBlock != null) {
                                undoBlockExecution(executedBlock);
                            }
                        }
                    }
                    for (int i = branch.size() - 1; i >= 0; i--) {
                        block = branch.get(i);
                        if (!block.getIsMain()) {
                            executeBlock(block);
                        }
                    }
                    blockService.updateHighestBlock();
                }
            }
            sleep();
        }
    }

    private void executeBlock(Block block) {
        Wallet minerWallet = walletRepository.findWalletByPublicKeyHash(Hash.sha256(block.getPublicKey()));
        if (minerWallet == null) {
            minerWallet = new Wallet(block.getPublicKey());
        }
        minerWallet.setBalance(minerWallet.getBalance().add(block.getBlockReward()));
        minerWallet.setLastOperation(block.getHeight());
        block.setIsMain(true);
        walletRepository.save(minerWallet);
        blockRepository.save(block);
    }

    private void undoBlockExecution(Block block) {
        Wallet minerWallet = walletRepository.findWalletByPublicKeyHash(Hash.sha256(block.getPublicKey()));
        minerWallet.setBalance(minerWallet.getBalance().subtract(block.getBlockReward()));
        block.setIsMain(false);
        walletRepository.save(minerWallet);
        blockRepository.save(block);
    }

    private void sleep() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(BlockchainMainThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
