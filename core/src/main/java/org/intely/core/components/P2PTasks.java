package org.intely.core.components;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.intely.core.commandline.DebugCommandLine;
import org.intely.core.configs.P2PConfigs;
import org.intely.core.configs.SetupConfigs;
import org.intely.core.domain.Node;
import org.intely.core.dto.InfoDTO;
import org.intely.core.exception.CantConnectException;
import org.intely.core.exception.DisconectedNodeException;
import org.intely.core.exception.WalletNodeException;
import org.intely.core.repositories.NodeRepository;
import org.intely.core.services.NodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class P2PTasks {

    @Autowired
    private NodeRepository nodeRepository;

    @Autowired
    private NodeService nodeService;

    @Scheduled(fixedRate = P2PConfigs.SEARCH_NEW_NODES)
    public void checkNodes() throws IOException {
        if (nodeRepository.isSearching()) {
            Node randomNode = nodeService.getRandomNode();
            try {
                InfoDTO info = nodeService.consultNodeInfo(randomNode);
                if (!randomNode.getConnected()
                        && !SetupConfigs.getInstance().getName().equals(randomNode.getName())
                        && !info.getIsFull()) {
                    Logger.getLogger(P2PTasks.class.getName()).log(Level.INFO, "Try conected new node {0}", randomNode.toString());
                    try {
                        nodeService.tryConnectNode(randomNode);
                        Logger.getLogger(P2PTasks.class.getName()).log(Level.INFO, "Conected new node {0}", randomNode.toString());
                    } catch (CantConnectException ex) {
                        Logger.getLogger(P2PTasks.class.getName()).log(Level.INFO, null, ex);
                    }
                }
            } catch (DisconectedNodeException ex) {
                Logger.getLogger(P2PTasks.class.getName()).log(Level.INFO, ex.getMessage());
            } catch (WalletNodeException ex) {
            }
        }
    }

    @Scheduled(fixedRate = P2PConfigs.TOKEN_REFRESH)
    public void updateTokenNodes() throws IOException {
        nodeRepository.getConnectedNodes().forEach(connectedNode -> {
            try {
                nodeService.tryConnectNode(connectedNode);
            } catch (CantConnectException ex) {
                Logger.getLogger(P2PTasks.class.getName()).log(Level.INFO, ex.getMessage());
            } catch (WalletNodeException ex) {
            }
        });
    }
}
