package org.intely.core.components;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.intely.core.configs.SetupConfigs;
import org.intely.core.domain.Block;
import org.intely.core.dto.BlockDTO;
import org.intely.core.exception.InvalidFieldsException;
import org.intely.core.helpers.ByteUtils;
import org.intely.core.helpers.Hash;
import org.intely.core.services.BlockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@EnableScheduling
@Component
public class MiningThread implements Runnable {

    private static boolean needToRunStartupMethod = true;

    private long nonce = 0;

    @Autowired
    private BlockService service;

    @Scheduled(fixedRate = 3600000)
    public void startMiner() {
        if (needToRunStartupMethod) {
            needToRunStartupMethod = false;
            Thread thread = new Thread(this);
            thread.setName(this.getClass().getSimpleName());
            thread.start();
        }
    }

    @Override
    public void run() {
        service.verifyBlockZero();
        while (SetupConfigs.getInstance().getMinerBlockEnable()) {
            byte[] publicKey = Wallets.getInstance().getMinerPublicKey();
            Block blockToBeMined = service.getBlockToBeMinned(publicKey);
            Block minnedBlock = tryGenerateBlock(10000, blockToBeMined);
            if (minnedBlock != null) {
                System.out.println("Generate new Block");
                service.publishBlock(minnedBlock);
            }
        }
    }

    private Block tryGenerateBlock(int tries, Block block) {
        byte[] hash = (new BlockDTO(block)).preHash();
        hash = Arrays.copyOf(hash, hash.length + Long.BYTES);
        BigInteger target = block.calcTarget();
        BigInteger newHashInteger;
        byte[] newHash;
        int count = 0;
        do {
            count++;
            nonce++;
            System.arraycopy(ByteUtils.longToBytes(nonce), 0, hash, 32, Long.BYTES);
            newHash = Hash.X12(hash);
            newHashInteger = new BigInteger(newHash);
        } while (newHashInteger.compareTo(target) != -1 && count < tries);
        if (newHashInteger.compareTo(target) != -1) {
            return null;
        } else {
            block.setNonce(nonce);
            block.setBlockHash(newHash);
            return block;
        }
    }
}
