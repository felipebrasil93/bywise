package org.intely.core.components;

import java.io.File;
import java.util.ArrayList;
import java.util.Base64;
import org.apache.commons.codec.binary.Hex;
import org.intely.core.domain.MyWallet;
import org.intely.core.helpers.Hash;
import org.intely.core.helpers.HelperFile;
import org.intely.core.helpers.WalletTools;
import org.json.JSONArray;
import org.json.JSONObject;

public class Wallets {

    private static final String MINER_PUBLIC_KEY_FILE = "minerPublicKey.json";
    private static final String MINER_PRIVATE_KEY_FILE = "minerPrivateKey.json";
    private static final String WALLETS_FILE = "wallets.json";
    private static Wallets INSTANCE = null;

    public synchronized static Wallets getInstance() {
        if (INSTANCE == null) {
            File walletsFile = new File(MINER_PUBLIC_KEY_FILE);
            if (!walletsFile.exists()) {
                INSTANCE = new Wallets();
                INSTANCE.saveWallets();
            } else {
                INSTANCE = new Wallets();
                INSTANCE.loadWallets();
            }
        }
        return INSTANCE;
    }

    private byte[] minerPublicKey = null;
    private byte[] minerPrivateKey = null;

    public Wallets() {
        MyWallet minnerWallet = WalletTools.generateWallet();
        this.minerPublicKey = minnerWallet.getPublicKey();
        this.minerPrivateKey = minnerWallet.getPrivateKey();
    }

    public byte[] getMinerPublicKey() {
        return minerPublicKey;
    }

    public byte[] getMinerPrivateKey() {
        return minerPrivateKey;
    }

    private void saveWallets() {
        JSONArray array = new JSONArray();
        JSONObject json = new JSONObject();
        json.put("privateKey", "ok-" + Hex.encodeHexString(minerPrivateKey));
        json.put("publicKey", Hex.encodeHexString(minerPublicKey));
        json.put("publicKeyHash", Hex.encodeHexString(Hash.sha256(minerPublicKey)));
        array.put(json);
        for (int i = 0; i < 99; i++) {
            MyWallet myWallet = WalletTools.generateWallet();
            json = new JSONObject();
            json.put("privateKey", "ok-" + Hex.encodeHexString(myWallet.getPrivateKey()));
            json.put("publicKey", Hex.encodeHexString(myWallet.getPublicKey()));
            json.put("publicKeyHash", Hex.encodeHexString(myWallet.getPublicKeyHash()));
            array.put(json);
        }
        HelperFile.saveFile(new File(WALLETS_FILE), array.toString());

        json = new JSONObject();
        json.put("minerPublicKey", Base64.getEncoder().encodeToString(minerPublicKey));
        HelperFile.saveFile(new File(MINER_PUBLIC_KEY_FILE), json.toString());
        
        json = new JSONObject();
        json.put("minerPrivateKey", Base64.getEncoder().encodeToString(minerPrivateKey));
        HelperFile.saveFile(new File(MINER_PRIVATE_KEY_FILE), json.toString());
    }

    private void loadWallets() {
        File f = new File(MINER_PUBLIC_KEY_FILE);
        if (f.exists()) {
            String strJson = HelperFile.readFile(f, true);
            if (strJson != null) {
                JSONObject json = new JSONObject(strJson);
                minerPublicKey = Base64.getDecoder().decode(json.getString("minerPublicKey"));
            }
        }
        
        f = new File(MINER_PRIVATE_KEY_FILE);
        if (f.exists()) {
            String strJson = HelperFile.readFile(f, true);
            if (strJson != null) {
                JSONObject json = new JSONObject(strJson);
                minerPrivateKey = Base64.getDecoder().decode(json.getString("minerPrivateKey"));
            }
        }
    }
}
