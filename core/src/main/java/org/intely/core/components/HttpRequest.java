package org.intely.core.components;

import com.google.gson.Gson;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.intely.core.configs.P2PConfigs;
import org.intely.core.domain.Node;
import org.intely.core.domain.enums.NodeTypeEnum;
import org.intely.core.exception.DisconectedNodeException;
import org.intely.core.exception.WalletNodeException;
import org.intely.core.repositories.NodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Component
public class HttpRequest {

    @Autowired
    private NodeRepository nodeRepository;

    public static final String PATH = "/server/v1";
    public static final Gson GSON = new Gson();

    public void get(String endPoint, Node node) throws DisconectedNodeException, WalletNodeException {
        node = nodeRepository.getKnownNode(node);
        RestTemplate rest = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Accept", "*/*");
        if (node.getToken() != null) {
            headers.add("Authorization", node.getToken());
        }
        makeRequest(node, endPoint, "", headers, rest, HttpMethod.GET, 0, null);
    }

    public <T extends Object> T get(String endPoint, Node node, Class<T> valueType) throws DisconectedNodeException, WalletNodeException {
        node = nodeRepository.getKnownNode(node);
        RestTemplate rest = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Accept", "*/*");
        if (node.getToken() != null) {
            headers.add("Authorization", node.getToken());
        }
        return makeRequest(node, endPoint, "", headers, rest, HttpMethod.GET, 0, valueType);
    }

    public void post(String endPoint, Node node, String bodyRequest) throws DisconectedNodeException, WalletNodeException {
        node = nodeRepository.getKnownNode(node);
        RestTemplate rest = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Accept", "*/*");
        if (node.getToken() != null) {
            headers.add("Authorization", node.getToken());
        }
        makeRequest(node, endPoint, bodyRequest, headers, rest, HttpMethod.POST, 0, null);
    }

    public <T extends Object> T post(String endPoint, Node node, String bodyRequest, Class<T> valueType) throws DisconectedNodeException, WalletNodeException {
        node = nodeRepository.getKnownNode(node);
        RestTemplate rest = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Accept", "*/*");
        if (node.getToken() != null) {
            headers.add("Authorization", node.getToken());
        }
        return makeRequest(node, endPoint, bodyRequest, headers, rest, HttpMethod.POST, 0, valueType);
    }

    private <T extends Object> T makeRequest(Node node, String endPoint, String bodyRequest, HttpHeaders headers, RestTemplate rest, HttpMethod method, int countTries, Class<T> valueType) throws DisconectedNodeException, WalletNodeException {
        if(node.getType().equals(NodeTypeEnum.WALLET)){
            throw new WalletNodeException();
        }
        try {
            countTries++;
            if (countTries > 3) {
                nodeRepository.disconectNode(node);
                throw new DisconectedNodeException("Cant connect node " + node.getId());
            }
            try {
                node.makeRequest();
                String url = "http://" + node.getAddress() + ":" + node.getPort() + PATH + endPoint;
                Logger.getLogger(HttpRequest.class.getName()).log(Level.INFO, "Request {0}", new Object[]{url});
                HttpEntity<String> requestEntity = new HttpEntity(bodyRequest, headers);
                ResponseEntity<T> responseEntity = rest.exchange(url, method, requestEntity, valueType);
                if (valueType == null) {
                    return null;
                } else {
                    return responseEntity.getBody();
                }
            } catch (HttpClientErrorException ex) {
                Logger.getLogger(HttpRequest.class.getName()).log(Level.INFO, "Status code {0} {1} {2}", new Object[]{node.getId(), ex.getStatusText(), ex.getStatusCode().value()});
                Logger.getLogger(HttpRequest.class.getName()).log(Level.INFO, ex.getResponseBodyAsString());
                switch (ex.getStatusCode().value()) {
                    case 404:
                        return null;
                    case 403:
                        if (node.getMinDelay() != null) {
                            Thread.sleep(node.getMinDelay() * 2);
                        }
                        break;
                    default:
                        Thread.sleep(P2PConfigs.RETRY_ERROR_DELAY);
                        break;
                }
                return makeRequest(node, endPoint, bodyRequest, headers, rest, method, countTries, valueType);
            } catch (Exception ex) {
                Logger.getLogger(HttpRequest.class.getName()).log(Level.INFO, "Connection fail {0} {1} {2}", new Object[]{HttpRequest.class.getSimpleName(), node.getId(), ex});
                Thread.sleep(P2PConfigs.RETRY_DELAY);
                return makeRequest(node, endPoint, bodyRequest, headers, rest, method, countTries, valueType);
            }
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex);
        }
    }
}
