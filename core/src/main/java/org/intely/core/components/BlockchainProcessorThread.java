package org.intely.core.components;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.intely.core.domain.Block;
import org.intely.core.exception.DisconectedNodeException;
import org.intely.core.exception.InvalidFieldsException;
import org.intely.core.repositories.BlockRepository;
import org.intely.core.services.BlockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@EnableScheduling
@Component
public class BlockchainProcessorThread implements Runnable {

    private static boolean needToRunStartupMethod = true;

    @Autowired
    private BlockRepository blockRepository;
    @Autowired
    private BlockService blockService;

    @Scheduled(fixedRate = 3600000)
    public void startBlockchainProcessor() {
        if (needToRunStartupMethod) {
            needToRunStartupMethod = false;
            Thread thread = new Thread(this);
            thread.setName(this.getClass().getSimpleName());
            thread.start();
        }
    }

    @Override
    public void run() {
        while (true) {
            Block block = blockService.getNotProcessedBlock();
            if (block != null) {
                block.setIsProcessed(true);
                boolean isValidated = false;
                if (Arrays.equals(block.getPreviousBlockHash(), blockService.getBlockZero().getPreviousBlockHash())
                        || Arrays.equals(block.getPreviousBlockHash(), blockService.getBlockZero().getBlockHash())) {
                    isValidated = true;
                } else {
                    Block previousBlock = blockRepository.findBlockByHash(block.getPreviousBlockHash());
                    if (previousBlock != null) {
                        isValidated = true;
                    } else {
                        try {
                            Block[] blocks = blockService.searchBlockchainByHash(block.getPreviousBlockHash(), 10);
                            if (blocks != null) {
                                for (Block b : blocks) {
                                    Block savedBlock = blockRepository.findBlockByHash(b.getBlockHash());
                                    if (savedBlock == null) {
                                        //b.validate();
                                        blockRepository.save(b);
                                        if (Arrays.equals(block.getPreviousBlockHash(), b.getBlockHash())) {
                                            isValidated = true;
                                        }
                                    }
                                }
                            }
                        } catch (DisconectedNodeException ex) {
                            Logger.getLogger(BlockchainProcessorThread.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                block.setIsValidated(isValidated);
                blockRepository.save(block);
                if (isValidated) {
                    blockService.updateHighestBlock();
                }
            }
            sleep();
        }
    }

    private void sleep() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(BlockchainProcessorThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
