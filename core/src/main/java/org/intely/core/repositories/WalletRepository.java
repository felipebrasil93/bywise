package org.intely.core.repositories;

import org.intely.core.domain.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface WalletRepository extends JpaRepository<Wallet, Long>{
    
    @Query(value = "SELECT * FROM Wallet WHERE username = ?1 AND balance > 0 LIMIT 1", nativeQuery = true)
    public Wallet findWalletByUsername(String username);
    
    @Query(value = "SELECT * FROM Wallet WHERE public_key_hash = ?1 LIMIT 1", nativeQuery = true)
    public Wallet findWalletByPublicKeyHash(byte[] publicKeyHash);
    
}
