package org.intely.core.repositories;

import org.intely.core.domain.Debit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionEntryRepository extends JpaRepository<Debit, Long>{
    
}
