package org.intely.core.repositories;

import org.intely.core.domain.Block;
import org.intely.core.domain.Deposit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface DepositRepository extends JpaRepository<Deposit, Long>{
    
    @Query(value = "SELECT * FROM Deposit WHERE block_hash = ?1 LIMIT 1", nativeQuery = true)
    public Block findDepositByCreditedPublicKeyHash(byte[] blockHash);
}
