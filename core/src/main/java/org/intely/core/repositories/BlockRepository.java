package org.intely.core.repositories;

import java.util.List;
import org.intely.core.domain.Block;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BlockRepository extends JpaRepository<Block, Long> {

    @Query(value = "SELECT * FROM Block WHERE height = ?1 AND is_main = true ORDER BY id ASC LIMIT 1", nativeQuery = true)
    public Block findMainBlockByHeight(Long height);
    
    @Query(value = "SELECT * FROM Block WHERE height = ?1", nativeQuery = true)
    public List<Block> findBlocksByHeight(Long height);
    
    @Query(value = "SELECT * FROM Block WHERE block_hash = ?1 LIMIT 1", nativeQuery = true)
    public Block findBlockByHash(byte[] blockHash);
    
    @Query(value = "SELECT * FROM Block WHERE is_main = true ORDER BY height DESC LIMIT 1", nativeQuery = true)
    public Block highestHeight();
    
    @Query(value = "SELECT * FROM Block WHERE is_processed = false LIMIT 100", nativeQuery = true)
    public List<Block> findBlockNotProcessed();
    
    @Query(value = "SELECT * FROM Block WHERE is_validated = true AND is_main = false ORDER BY height DESC LIMIT 1", nativeQuery = true)
    public Block findHighestBlockNotMain();
}
