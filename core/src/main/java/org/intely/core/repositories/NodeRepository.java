package org.intely.core.repositories;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.intely.core.configs.LimitsConfigs;
import org.intely.core.configs.P2PConfigs;
import org.intely.core.configs.SetupConfigs;
import org.intely.core.domain.Node;
import org.intely.core.exception.CantConnectException;
import org.springframework.stereotype.Component;

@Component
public class NodeRepository {

    private final ArrayList<Node> conectedNodes = new ArrayList();
    private final ArrayList<Node> nodes = new ArrayList();

    public NodeRepository() {
        nodes.addAll(Arrays.asList(SetupConfigs.initialKnownNodes()));
    }

    public Node getConectedNode(String id) {
        synchronized (NodeRepository.class) {
            for (Node n : conectedNodes) {
                if (n.getId().equals(id)) {
                    return n;
                }
            }
            return null;
        }
    }

    public Node getKnownNode(Node node) {
        synchronized (NodeRepository.class) {
            for (Node n : nodes) {
                if (n.getId().equals(node.getId())) {
                    return n;
                }
            }
        }
        addNode(node);
        return node;
    }

    public ArrayList<Node> getConnectedNodes() {
        synchronized (NodeRepository.class) {
            ArrayList<Node> nds = new ArrayList<>();
            conectedNodes.forEach((n) -> {
                if (n.getToken() != null) {
                    nds.add(n);
                }
            });
            return nds;
        }
    }

    public ArrayList<Node> getNodes() {
        synchronized (NodeRepository.class) {
            ArrayList<Node> nds = new ArrayList<>();
            nodes.forEach((n) -> {
                if (!n.getDisconnected()) {
                    nds.add(n);
                }
            });
            return nds;
        }
    }

    public void addNode(Node node) {
        synchronized (NodeRepository.class) {
            boolean repeted = false;
            for (Node n : nodes) {
                if (n.equals(node)) {
                    repeted = true;
                }
            }
            if (!repeted) {
                if (nodes.size() >= LimitsConfigs.LIMIT_LIST_NODES) {
                    Node n = nodes.remove(0);
                    conectedNodes.remove(n);
                }
                nodes.add(node);
                Logger.getLogger(NodeRepository.class.getName()).log(Level.INFO, "Found new node {0}", node.toString());
            }
        }
    }

    private void banNodes() {
        for (int i = conectedNodes.size() - 1; i >= 0; i--) {
            Node node = conectedNodes.get(i);
            if (node.isBan()) {
                conectedNodes.remove(i);
            }
        }
    }

    public boolean isFull() {
        synchronized (NodeRepository.class) {
            banNodes();
            return conectedNodes.size() >= SetupConfigs.getInstance().getMaxConectedNodes();
        }
    }

    public boolean isSearching() {
        synchronized (NodeRepository.class) {
            banNodes();
            return conectedNodes.size() < SetupConfigs.getInstance().getMinConectedNodes();
        }
    }

    public void connectNode(Node newNode) throws CantConnectException {
        addNode(newNode);
        if (!SetupConfigs.VERSION.equals(newNode.getVersion())) {
            throw new CantConnectException("This node does not support this version");
        }
        if (!SetupConfigs.getInstance().getNetwork().equals(newNode.getNetwork())) {
            throw new CantConnectException("Unable to connect to " + newNode.getNetwork() + " network");
        }
        if (newNode.getMinDelay() == null || newNode.getMinDelay() >= P2PConfigs.MAX_DELAY || newNode.getMinDelay() < 0) {
            throw new CantConnectException("This node has no invalid minimum delay");
        }
        synchronized (NodeRepository.class) {
            banNodes();
            Node connectedNode = getConectedNode(newNode.getId());
            if (conectedNodes.size() < SetupConfigs.getInstance().getMaxConectedNodes() || connectedNode != null) {
                if (connectedNode != null) {
                    if (connectedNode.isBan()) {
                        throw new CantConnectException("Your node was banned");
                    } else {
                        connectedNode.updateNode(newNode);
                        connectedNode.setConnected(true);
                        Logger.getLogger(NodeRepository.class.getName()).log(Level.INFO, "Refresh node {0}", connectedNode.toString());
                        return;
                    }
                } else {
                    Node knownNode = getKnownNode(newNode);
                    if (knownNode != null) {
                        if (knownNode.isBan()) {
                            throw new CantConnectException("Your node was banned");
                        } else {
                            knownNode.updateNode(newNode);
                            knownNode.setConnected(true);
                            conectedNodes.add(knownNode);
                            Logger.getLogger(NodeRepository.class.getName()).log(Level.INFO, "Add new node {0}", knownNode.toString());
                            return;
                        }
                    }
                }
            } else {
                throw new CantConnectException("This node is full");
            }
        }
        throw new CantConnectException("Unknown error");
    }

    public void removeConnectedNode(Node node) {
        addNode(node);
        synchronized (NodeRepository.class) {
            conectedNodes.remove(node);
            node.setConnected(false);
            Logger.getLogger(NodeRepository.class.getName()).log(Level.INFO, "Revoke node {0}", node.toString());
        }
    }

    public void disconectNode(Node node) {
        synchronized (NodeRepository.class) {
            conectedNodes.remove(node);
            node.setConnected(false);
            node.setDisconnected(true);
            Logger.getLogger(NodeRepository.class.getName()).log(Level.INFO, "Disconected node {0}", node.toString());
        }
    }
}
