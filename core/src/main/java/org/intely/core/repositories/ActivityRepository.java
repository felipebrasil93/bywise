package org.intely.core.repositories;

import org.intely.core.domain.Activity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActivityRepository extends JpaRepository<Activity, Long>{
    
}
