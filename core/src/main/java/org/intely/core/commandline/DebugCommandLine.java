package org.intely.core.commandline;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.math.BigDecimal;
import org.intely.core.domain.Block;
import org.intely.core.domain.MyWallet;
import org.intely.core.domain.Debit;
import org.intely.core.helpers.WalletTools;
import org.intely.core.repositories.TransactionEntryRepository;
import org.intely.core.services.BlockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellCommandGroup;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

@ShellComponent
@ShellCommandGroup("Debug")
public class DebugCommandLine {

    public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    
    @Autowired
    BlockService s;
    
    @ShellMethod("Debug function")
    public String debug() {
        Block b = s.getBlockZero();
        s.publishBlock(b);
        return "";
    }

    @ShellMethod("Debug function 2")
    public String debug2() {
        return "";
    }

}
