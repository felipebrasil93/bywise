package org.intely.core.commandline;

import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.intely.core.domain.Node;
import org.intely.core.exception.DisconectedNodeException;
import org.intely.core.exception.WalletNodeException;
import org.intely.core.repositories.NodeRepository;
import org.intely.core.services.NodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellCommandGroup;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

@ShellComponent
@ShellCommandGroup("Network")
public class NetworkCommandLine {

    @Autowired
    private NodeRepository repository;
    @Autowired
    private NodeService nodeService;

    @ShellMethod("List conected nodes")
    public String connectedNodes() {
        String s = "";
        int count = 0;
        for (Node node : repository.getConnectedNodes()) {
            s += count+"\t "+node.toString() + "\n";
            count++;
        }
        return s;
    }

    @ShellMethod("List known nodes")
    public String allNodes() {
        String s = "";
        int count = 0;
        for (Node node : repository.getNodes()) {
            s += count+"\t "+node.toString() + "\n";
            count++;
        }
        return s;
    }

    @ShellMethod("Ping")
    public String ping() {
        List<Node> nodes = repository.getNodes();
        Random rd = new Random();
        if(!nodes.isEmpty()){
            try {
                Node node = nodes.get(rd.nextInt(nodes.size()));
                nodeService.pingNode(node);
                return "Sucess";
            } catch (DisconectedNodeException | WalletNodeException ex) {
                return "Fail "+ex.getMessage();
            }
        }
        return "Fail";
    }
}
