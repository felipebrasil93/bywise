package org.intely.core.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import javax.persistence.*;
import lombok.*;
import org.intely.core.exception.InvalidFieldsException;
import org.intely.core.helpers.Hash;
import org.intely.core.interfaces.ValidateInterface;

@NoArgsConstructor
@Getter
@Setter
@Entity
public class PackageDTO implements ValidateInterface {

    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private byte[] publicKey;
    private byte[] previousPackageHash;
    @Column(precision = 25, scale = 15)
    private String total;
    @Column(precision = 25, scale = 15)
    private String totalFee;
    private Long created;
    private Integer packageSize;
    private List<byte[]> activitiesHash;
    private byte[] packageHash;
    private byte[] signature;
    
    public byte[] toHash() {
        try {
            ByteArrayOutputStream bytesOutput = new ByteArrayOutputStream();
            bytesOutput.write(publicKey);
            bytesOutput.write(previousPackageHash);
            bytesOutput.write(formatValue(total));
            bytesOutput.write(formatValue(totalFee));
            bytesOutput.write(formatValue(created));
            bytesOutput.write(formatValue(packageSize));
            bytesOutput.write(signature);
            return Hash.sha256(bytesOutput.toByteArray());
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void validate() throws InvalidFieldsException {
        validadeValue(total, "total");
        validadeValue(totalFee, "totalFee");
        if (publicKey == null || publicKey.length != 162) {
            throw new InvalidFieldsException("Invalid publicKeyHash");
        }
        if (previousPackageHash == null || previousPackageHash.length != 64) {
            throw new InvalidFieldsException("Invalid previousPackageHash");
        }
        if (total == null || (new BigDecimal(total)).compareTo(BigDecimal.ZERO) == -1) {
            throw new InvalidFieldsException("Invalid total \"" + total + "\"");
        }
        if (totalFee == null || (new BigDecimal(totalFee)).compareTo(BigDecimal.ZERO) == -1) {
            throw new InvalidFieldsException("Invalid totalFee \"" + totalFee + "\"");
        }
        if (created == null || created <= 0) {
            throw new InvalidFieldsException("Invalid created \"" + created + "\"");
        }
        if (packageSize == null || packageSize <= 0) {
            throw new InvalidFieldsException("Invalid packageSize \"" + packageSize + "\"");
        }
        if (activitiesHash == null || activitiesHash.size() != packageSize) {
            throw new InvalidFieldsException("Invalid activitiesHash \"" + activitiesHash + "\"");
        }
        if (packageHash == null || packageHash.length != 64) {
            throw new InvalidFieldsException("Invalid signature");
        }
        if (signature == null || signature.length != 128) {
            throw new InvalidFieldsException("Invalid signature");
        }
    }

}
