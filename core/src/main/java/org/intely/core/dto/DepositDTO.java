package org.intely.core.dto;

import org.intely.core.domain.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import lombok.*;
import org.intely.core.exception.InvalidFieldsException;
import org.intely.core.helpers.Hash;
import org.intely.core.interfaces.ValidateInterface;

@Data
@EqualsAndHashCode(exclude = "inputs")

@NoArgsConstructor
@Getter
@Setter
public class DepositDTO implements ValidateInterface {

    private byte[] creditedPublicKeyHash;
    private String balanceValue;
    private String feeValue;
    private Long created;
    private Integer inputsSize;
    private List<DebitDTO> inputs;

    public Deposit toDeposit() {
        Deposit d = new Deposit();
        d.setCreditedPublicKeyHash(creditedPublicKeyHash);
        d.setBalanceValue(new BigDecimal(balanceValue));
        d.setFeeValue(new BigDecimal(feeValue));
        d.setCreated(created);
        ArrayList<Debit> inputsTemp = new ArrayList();
        inputs.forEach((input) -> {
            inputsTemp.add(input.toTransactionEntry());
        });
        d.setInputs(inputsTemp);
        d.setHashActivity(toHash());
        return d;
    }

    public byte[] preHash() {
        try {
            ByteArrayOutputStream bytesOutput = new ByteArrayOutputStream();
            bytesOutput.write(creditedPublicKeyHash);
            bytesOutput.write(formatValue(balanceValue));
            bytesOutput.write(formatValue(feeValue));
            bytesOutput.write(formatValue(created));
            return Hash.sha512(Hash.sha512(bytesOutput.toByteArray()));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public byte[] toHash() {
        try {
            ByteArrayOutputStream bytesOutput = new ByteArrayOutputStream();
            bytesOutput.write(creditedPublicKeyHash);
            bytesOutput.write(formatValue(balanceValue));
            bytesOutput.write(formatValue(feeValue));
            bytesOutput.write(formatValue(created));
            for (int i = 0; i < inputs.size(); i++) {
                for (DebitDTO te : inputs) {
                    if (te.getPosition() == i) {
                        bytesOutput.write(te.toHash());
                    }
                }
            }
            return Hash.sha512(Hash.sha512(bytesOutput.toByteArray()));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void validate() throws InvalidFieldsException {
        validadeValue(balanceValue, "balanceValue");
        validadeValue(feeValue, "feeValue");
        if (creditedPublicKeyHash == null || creditedPublicKeyHash.length != 32) {
            throw new InvalidFieldsException("Invalid creditedPublicKeyHash");
        }
        if (new BigDecimal(balanceValue).compareTo(BigDecimal.ZERO) != 1) {
            throw new InvalidFieldsException("Invalid balanceValue \"" + balanceValue + "\"");
        }
        if (new BigDecimal(feeValue).compareTo(BigDecimal.ZERO) != 1) {
            throw new InvalidFieldsException("Invalid balanceValue \"" + balanceValue + "\"");
        }
        if (created == null || created <= 0) {
            throw new InvalidFieldsException("Invalid created \"" + created + "\"");
        }
        if (inputs.isEmpty()) {
            throw new InvalidFieldsException("Inputs is empty");
        }
        if (inputsSize == null || inputsSize != inputs.size()) {
            throw new InvalidFieldsException("Invalid inputsSize \"" + inputsSize + "\"");
        }
        BigDecimal totalDebited = (new BigDecimal(balanceValue)).add(new BigDecimal(feeValue));
        BigDecimal totalInputs = BigDecimal.ZERO;
        for (DebitDTO te : inputs) {
            te.validate();
            te.validateSignature(this);
            totalInputs = totalInputs.add(new BigDecimal(te.getTxValue()));
        }
        if (totalDebited.compareTo(totalInputs) != 0) {
            throw new InvalidFieldsException("The debited value and the value of the entries do not match");
        }
        for (int i = 0; i < inputs.size(); i++) {
            boolean found = false;
            for (int j = 0; j < inputs.size() && !found; j++) {
                DebitDTO get = inputs.get(j);
                if (get.getPosition() == i) {
                    found = true;
                }
            }
            if (!found) {
                throw new InvalidFieldsException("Entry " + i + " was not found");
            }
        }
    }
}
