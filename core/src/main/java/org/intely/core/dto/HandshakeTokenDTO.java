package org.intely.core.dto;

import lombok.Getter;
import lombok.Setter;
import org.intely.core.configs.P2PConfigs;
import org.intely.core.configs.SetupConfigs;
import org.intely.core.domain.enums.NodeTypeEnum;
import org.intely.core.exception.InvalidFieldsException;
import org.intely.core.interfaces.ValidateInterface;

@Setter
@Getter
public class HandshakeTokenDTO implements ValidateInterface {

    private String message;
    private Boolean sucess;
    private String token;
    private String name;
    private NodeTypeEnum type;
    private String version;
    private String software;
    private Integer minDelay;
    private Long height;

    public HandshakeTokenDTO() {
        this.message = null;
        this.sucess = true;
    }

    public HandshakeTokenDTO(String message) {
        this.message = message;
        this.sucess = false;
    }

    @Override
    public void validate() throws InvalidFieldsException {
        if (sucess == null) {
            throw new InvalidFieldsException("Invalid sucess");
        }
        if (sucess) {
            if (token == null || !token.matches("^[A-Za-z0-9-_=]+\\.[A-Za-z0-9-_=]+\\.?[A-Za-z0-9-_.+/=]*$")) {
                throw new InvalidFieldsException("Invalid token regex \"" + token + "\"");
            }
            if (type == null) {
                throw new InvalidFieldsException("Invalid type");
            }
            if (name == null || !name.matches("^[\\w\\.\\-0-9]{0,40}$")) {
                throw new InvalidFieldsException("Invalid name \"" + name + "\"");
            }
            if (!SetupConfigs.getInstance().isSuportedVersion(version)) {
                throw new InvalidFieldsException("This node does not support this version \"" + version + "\"");
            }
            if (software == null || !software.matches("^[\\w\\.\\-0-9]{0,40}$")) {
                throw new InvalidFieldsException("Invalid software \"" + software + "\"");
            }
            if (minDelay == null || minDelay < 0 && minDelay > P2PConfigs.MAX_DELAY) {
                throw new InvalidFieldsException("Invalid minDelay \"" + minDelay + "\"");
            }
            if (height == null || height < 0) {
                throw new InvalidFieldsException("Invalid height \"" + height + "\"");
            }
        } else {
            if (message == null) {
                throw new InvalidFieldsException("Invalid message");
            }
        }
    }
}
