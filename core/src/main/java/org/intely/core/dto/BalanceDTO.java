package org.intely.core.dto;

import java.math.BigDecimal;
import lombok.*;

@NoArgsConstructor
@Getter
@Setter
public class BalanceDTO {

    private byte[] publicKeyHash;
    private BigDecimal balanceValue;
    private Long lastUse;

    public BalanceDTO(byte[] publicKeyHash, BigDecimal balanceValue, Long lastUse) {
        this.publicKeyHash = publicKeyHash;
        this.balanceValue = balanceValue;
        this.lastUse = lastUse;
    }

}
