package org.intely.core.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.intely.core.configs.P2PConfigs;
import org.intely.core.configs.SetupConfigs;
import org.intely.core.domain.Node;
import org.intely.core.domain.enums.NetworkEnum;
import org.intely.core.domain.enums.NodeTypeEnum;
import org.intely.core.exception.InvalidFieldsException;
import org.intely.core.interfaces.ValidateInterface;

@Getter
@Setter
@NoArgsConstructor
public class HandshakeDTO implements ValidateInterface {

    private String youToken;
    private String name;
    private Integer port;
    private NetworkEnum network;
    private NodeTypeEnum type;
    private String version;
    private String software;
    private Integer minDelay;
    private Long timestamp;
    private Long height;

    public HandshakeDTO(Node node) {
        this.name = node.getName();
        this.port = node.getPort();
        this.network = node.getNetwork();
        this.version = node.getVersion();
        this.software = node.getSoftware();
        this.minDelay = node.getMinDelay();
        this.type = node.getType();
    }

    @Override
    public void validate() throws InvalidFieldsException {
        if (type == null && type.equals(NodeTypeEnum.MINNER)) {
            throw new InvalidFieldsException("Invalid type");
        }
        if (youToken == null || !youToken.matches("^[A-Za-z0-9-_=]+\\.[A-Za-z0-9-_=]+\\.?[A-Za-z0-9-_.+/=]*$")) {
            throw new InvalidFieldsException("Invalid token regex \"" + youToken + "\"");
        }
        if (name == null || !name.matches("^[\\w\\.\\-0-9]{0,40}$")) {
            throw new InvalidFieldsException("Invalid name \"" + name + "\"");
        }
        if (port == null || port <= 0 && port >= 65536) {
            throw new InvalidFieldsException("Invalid port \"" + port + "\"");
        }
        if (!SetupConfigs.getInstance().getNetwork().equals(network)) {
            throw new InvalidFieldsException("Invalid network \"" + network + "\"");
        }
        if (!SetupConfigs.getInstance().isSuportedVersion(version)) {
            throw new InvalidFieldsException("This node does not support this version \"" + version + "\"");
        }
        if (software == null || !software.matches("^[\\w\\.\\-0-9]{0,40}$")) {
            throw new InvalidFieldsException("Invalid software \"" + software + "\"");
        }
        if (minDelay == null || minDelay < 0 && minDelay > P2PConfigs.MAX_DELAY) {
            throw new InvalidFieldsException("Invalid minDelay \"" + minDelay + "\"");
        }
        if (timestamp == null || timestamp < 0) {
            throw new InvalidFieldsException("Invalid timestamp \"" + timestamp + "\"");
        }
        if (height == null || height < 0) {
            throw new InvalidFieldsException("Invalid height \"" + height + "\"");
        }
    }
}
