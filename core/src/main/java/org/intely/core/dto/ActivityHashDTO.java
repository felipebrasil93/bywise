package org.intely.core.dto;

import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.intely.core.configs.LimitsConfigs;
import org.intely.core.configs.P2PConfigs;
import org.intely.core.configs.SetupConfigs;
import org.intely.core.domain.enums.NetworkEnum;
import org.intely.core.exception.InvalidFieldsException;
import org.intely.core.interfaces.ValidateInterface;

@Getter
@Setter
@NoArgsConstructor
public class ActivityHashDTO implements ValidateInterface {

    private Integer pos;
    private byte[] hash;
    private String name;
    private NetworkEnum network;
    private String version;
    private String software;
    private Integer minDelay;
    private Long timestamp;
    private Long height;
    private Boolean isFull;
    private List<NodeDTO> nodes;

    @Override
    public void validate() throws InvalidFieldsException {
        if (name == null || !name.matches("^[\\w\\.\\-0-9]{0,40}$")) {
            throw new InvalidFieldsException("Invalid name \"" + name + "\"");
        }
        if (!SetupConfigs.getInstance().getNetwork().equals(network)) {
            throw new InvalidFieldsException("Invalid network \"" + network + "\"");
        }
        if (!SetupConfigs.getInstance().isSuportedVersion(version)) {
            throw new InvalidFieldsException("This node does not support this version \"" + version + "\"");
        }
        if (software == null || !software.matches("^[\\w\\.\\-0-9]{0,40}$")) {
            throw new InvalidFieldsException("Invalid software \"" + software + "\"");
        }
        if (minDelay == null || minDelay < 0 && minDelay > P2PConfigs.MAX_DELAY) {
            throw new InvalidFieldsException("Invalid minDelay \"" + minDelay + "\"");
        }
        if (timestamp == null || timestamp < 0) {
            throw new InvalidFieldsException("Invalid timestamp \"" + timestamp + "\"");
        }
        if (isFull == null) {
            throw new InvalidFieldsException("Invalid isFull");
        }
        if (nodes == null || nodes.size() >= LimitsConfigs.LIMIT_LIST_NODES) {
            throw new InvalidFieldsException("Invalid timestamp \"" + timestamp + "\"");
        }
        for(NodeDTO nodeDTO : nodes){
            try {
                nodeDTO.validate();
            } catch (InvalidFieldsException ex) {
                throw new InvalidFieldsException("Invalid nodes list => "+ex.getMessage());
            }
        }
    }

}
