package org.intely.core.dto;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import lombok.*;
import org.intely.core.domain.Debit;
import org.intely.core.exception.InvalidFieldsException;
import org.intely.core.helpers.Hash;
import org.intely.core.helpers.WalletTools;
import org.intely.core.interfaces.ValidateInterface;

@NoArgsConstructor
@Getter
@Setter
public class DebitDTO implements ValidateInterface {

    private byte[] publicKey;
    private String txValue;
    private Integer position;
    private byte[] signature;

    public Debit toTransactionEntry() {
        Debit te = new Debit();
        te.setPublicKey(publicKey);
        te.setSignature(signature);
        te.setTxValue(new BigDecimal(txValue));
        te.setPosition(position);
        return te;
    }

    private byte[] preHash(byte[] preHashDeposit) {
        try {
            ByteArrayOutputStream bytesOutput = new ByteArrayOutputStream();
            bytesOutput.write(publicKey);
            bytesOutput.write(preHashDeposit);
            bytesOutput.write(formatValue(txValue));
            bytesOutput.write(formatValue(position));
            return Hash.sha512(Hash.sha512(bytesOutput.toByteArray()));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public byte[] toHash() {
        try {
            ByteArrayOutputStream bytesOutput = new ByteArrayOutputStream();
            bytesOutput.write(publicKey);
            bytesOutput.write(formatValue(txValue));
            bytesOutput.write(formatValue(position));
            bytesOutput.write(signature);
            return Hash.sha512(Hash.sha512(bytesOutput.toByteArray()));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void validate() throws InvalidFieldsException {
        validadeValue(txValue, "txValue");
        if (publicKey == null) {
            throw new InvalidFieldsException("Invalid publicKey " + publicKey);
        }
        if (publicKey.length != 162) {
            throw new InvalidFieldsException("Invalid publicKey " + publicKey.length);
        }
        if (signature == null || signature.length != 128) {
            throw new InvalidFieldsException("Invalid signature \"" + signature + "\"");
        }
        if ((new BigDecimal(txValue)).compareTo(BigDecimal.ZERO) != 1) {
            throw new InvalidFieldsException("Invalid txValue \"" + txValue + "\"");
        }
        if (position == null || position < 0) {
            throw new InvalidFieldsException("Invalid position \"" + position + "\"");
        }
    }
    
    public void validateSignature(DepositDTO deposit) throws InvalidFieldsException {
        try {
            if (!WalletTools.verify(preHash(deposit.preHash()), signature, publicKey)) {
                throw new InvalidFieldsException("Signature does not match");
            }
        } catch (Exception ex) {
            throw new InvalidFieldsException(ex.getMessage());
        }
    }
}
