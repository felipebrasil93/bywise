package org.intely.core.dto;

import lombok.*;
import org.intely.core.configs.P2PConfigs;
import org.intely.core.configs.SetupConfigs;
import org.intely.core.domain.Node;
import org.intely.core.domain.enums.NetworkEnum;
import org.intely.core.exception.InvalidFieldsException;
import org.intely.core.interfaces.ValidateInterface;

@Getter
@Setter
@NoArgsConstructor
public class NodeDTO implements ValidateInterface {
    
    private String name;
    private String id;
    private String address;
    private Integer port;
    private Integer minDelay;
    private String version;
    private String software;

    public NodeDTO(Node node) {
        this.name = node.getName();
        this.id = node.getId();
        this.address = node.getAddress();
        this.port = node.getPort();
        this.minDelay = node.getMinDelay();
        this.version = node.getVersion();
        this.software = node.getSoftware();
    }

    public Node toNode(NetworkEnum network) {
        Node n = new Node(network, address, port, name);
        n.setVersion(version);
        n.setSoftware(software);
        n.setMinDelay(minDelay);
        return n;
    }
    
    @Override
    public void validate() throws InvalidFieldsException {
        if (address == null || !address.matches("^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}$")) {
            throw new InvalidFieldsException("Invalid IP \"" + address + "\"");
        }
        if (port == null || port <= 0 && port >= 65536) {
            throw new InvalidFieldsException("Invalid port \"" + port + "\"");
        }
        if (id == null || !id.equals(address+":"+port)) {
            throw new InvalidFieldsException("Invalid id \"" + id + "\"");
        }
        if (name == null || !name.matches("^[\\w\\.\\-0-9]{0,40}$")) {
            throw new InvalidFieldsException("Invalid name \"" + name + "\"");
        }
        if (!SetupConfigs.getInstance().isSuportedVersion(version)) {
            throw new InvalidFieldsException("This node does not support this version \"" + version + "\"");
        }
        if (software == null || !software.matches("^[\\w\\.\\-0-9]{0,40}$")) {
            throw new InvalidFieldsException("Invalid software \"" + software + "\"");
        }
        if (minDelay == null || minDelay < 0 && minDelay > P2PConfigs.MAX_DELAY) {
            throw new InvalidFieldsException("Invalid minDelay \"" + minDelay + "\"");
        }
    }
}
