package org.intely.core.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.intely.core.configs.P2PConfigs;
import org.intely.core.configs.SetupConfigs;
import org.intely.core.domain.Node;
import org.intely.core.domain.enums.NetworkEnum;
import org.intely.core.domain.enums.NodeTypeEnum;
import org.intely.core.exception.InvalidFieldsException;
import org.intely.core.interfaces.ValidateInterface;

@Getter
@Setter
@NoArgsConstructor
public class WalletHandshakeDTO implements ValidateInterface {

    private NetworkEnum network;
    private NodeTypeEnum type;
    private String version;
    private String software;
    private Long timestamp;

    public WalletHandshakeDTO(Node node) {
        this.network = node.getNetwork();
        this.version = node.getVersion();
        this.software = node.getSoftware();
        this.type = node.getType();
    }

    @Override
    public void validate() throws InvalidFieldsException {
        if (type == null && type.equals(NodeTypeEnum.WALLET)) {
            throw new InvalidFieldsException("Invalid type");
        }
        if (!SetupConfigs.getInstance().getNetwork().equals(network)) {
            throw new InvalidFieldsException("Invalid network \"" + network + "\"");
        }
        if (!SetupConfigs.getInstance().isSuportedVersion(version)) {
            throw new InvalidFieldsException("This node does not support this version \"" + version + "\"");
        }
        if (software == null || !software.matches("^[\\w\\.\\-0-9]{0,40}$")) {
            throw new InvalidFieldsException("Invalid software \"" + software + "\"");
        }
        if (timestamp == null || timestamp < 0) {
            throw new InvalidFieldsException("Invalid timestamp \"" + timestamp + "\"");
        }
    }
}
