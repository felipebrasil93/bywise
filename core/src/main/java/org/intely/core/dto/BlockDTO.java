package org.intely.core.dto;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import lombok.*;
import org.intely.core.domain.Block;
import org.intely.core.exception.InvalidFieldsException;
import org.intely.core.helpers.ByteUtils;
import org.intely.core.helpers.Hash;
import org.intely.core.interfaces.ValidateInterface;

@Getter
@Setter
@NoArgsConstructor
public class BlockDTO implements ValidateInterface {

    private byte[] publicKey;
    private byte[] previousBlockHash;
    private String difficultBlock;
    private String difficultPackages;
    private Long height;
    private String coinsCreated;
    private String totalFee;
    private String reward;
    private String blockReward;
    private String packageReward;
    private Long created;
    private Integer blockSize;
    //private List<byte[]> packageHashs;
    private Long nonce;
    private byte[] blockHash;

    public byte[] preHash() {
        try {
            ByteArrayOutputStream bytesOutput = new ByteArrayOutputStream();
            bytesOutput.write(publicKey);
            bytesOutput.write(previousBlockHash);
            bytesOutput.write(formatValue(difficultBlock));
            bytesOutput.write(formatValue(difficultPackages));
            bytesOutput.write(formatValue(height));
            bytesOutput.write(formatValue(blockSize));
            bytesOutput.write(formatValue(totalFee));
            bytesOutput.write(formatValue(reward));
            bytesOutput.write(formatValue(blockReward));
            bytesOutput.write(formatValue(packageReward));
            bytesOutput.write(formatValue(created));
            /*for (byte[] packageHash : packageHashs) {
                bytesOutput.write(packageHash);
            }*/
            return Hash.sha256(bytesOutput.toByteArray());
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public byte[] calcBlockHash() {
        byte[] hash = preHash();
        hash = Arrays.copyOf(hash, hash.length + Long.BYTES);
        System.arraycopy(ByteUtils.longToBytes(nonce), 0, hash, 32, Long.BYTES);
        return Hash.X12(hash);
    }
    
    public BlockDTO(Block block) {
        publicKey = block.getPublicKey();
        previousBlockHash = block.getPreviousBlockHash();
        difficultBlock = ValidateInterface.VALUE_FORMAT.format(block.getDifficultBlock());
        difficultPackages = ValidateInterface.VALUE_FORMAT.format(block.getDifficultPackages());
        height = block.getHeight();
        coinsCreated = ValidateInterface.VALUE_FORMAT.format(block.getCoinsCreated());
        totalFee = ValidateInterface.VALUE_FORMAT.format(block.getTotalFee());
        reward = ValidateInterface.VALUE_FORMAT.format(block.getReward());
        blockReward = ValidateInterface.VALUE_FORMAT.format(block.getBlockReward());
        packageReward = ValidateInterface.VALUE_FORMAT.format(block.getPackageReward());
        created = block.getCreated();
        blockSize = block.getBlockSize();
        nonce = block.getNonce();
        blockHash = block.getBlockHash();
    }

    public Block toBlock() {
        Block block = new Block();
        block.setPublicKey(publicKey);
        block.setPreviousBlockHash(previousBlockHash);
        block.setDifficultBlock(new BigDecimal(difficultBlock));
        block.setDifficultPackages(new BigDecimal(difficultPackages));
        block.setHeight(height);
        block.setCoinsCreated(new BigDecimal(coinsCreated));
        block.setTotalFee(new BigDecimal(totalFee));
        block.setReward(new BigDecimal(reward));
        block.setBlockReward(new BigDecimal(blockReward));
        block.setPackageReward(new BigDecimal(packageReward));
        block.setCreated(created);
        block.setBlockSize(blockSize);
        block.setNonce(nonce);
        block.setBlockHash(blockHash);
        return block;
    }

    @Override
    public void validate() throws InvalidFieldsException {
        validadeValue(totalFee, "totalFee");
        validadeValue(coinsCreated, "coinsCreated");
        validadeValue(reward, "reward");
        validadeValue(blockReward, "blockReward");
        validadeValue(packageReward, "packageReward");
        validadeValue(difficultBlock, "difficultBlock", 5, 40);
        validadeValue(difficultPackages, "difficultPackages", 5, 40);
        /*if (publicKey == null) {
            throw new InvalidFieldsException("Invalid publicKey");
        }
        if (previousBlockHash == null || previousBlockHash.length != 32) {
            throw new InvalidFieldsException("Invalid previousMasterHash");
        }
        if (difficultBlock == null || difficultBlock.compareTo(BigDecimal.ONE) == -1) {
            throw new InvalidFieldsException("Invalid difficultBlock \"" + difficultBlock + "\"");
        }
        if (difficultPackages == null || difficultPackages.compareTo(BigDecimal.ONE) == -1) {
            throw new InvalidFieldsException("Invalid difficultPackages \"" + difficultPackages + "\"");
        }
        if (height == null || height < 0) {
            throw new InvalidFieldsException("Invalid height \"" + height + "\"");
        }
        if (blockSize == null || blockSize < 0 || blockSize > LIMIT_PACKAGES) {
            throw new InvalidFieldsException("Invalid blockSize \"" + blockSize + "\"");
        }
        if (totalFee == null || totalFee.compareTo(BigDecimal.ZERO) == -1) {
            throw new InvalidFieldsException("Invalid blockSize \"" + blockSize + "\"");
        }
        if (coinsCreated.compareTo(BigDecimal.ZERO) == -1 || coinsCreated.compareTo(LIMIT_AMOUNT) == 1) {
            throw new InvalidFieldsException("Invalid coinsCreated \"" + coinsCreated + "\"");
        }
        if (reward.compareTo(calcMiningReward(height, coinsCreated, totalFee, blockSize)) != 0) {
            throw new InvalidFieldsException("Invalid reward \"" + reward + "\"");
        }
        if (blockReward.compareTo(calcBlockReward(reward, blockSize)) != 0) {
            throw new InvalidFieldsException("Invalid blockReward \"" + blockReward + "\"");
        }
        if (packageReward.compareTo(calcPackageReward(blockSize)) != 0) {
            throw new InvalidFieldsException("Invalid packageReward \"" + packageReward + "\"");
        }
        if (created == null) {
            throw new InvalidFieldsException("Invalid created");
        }
        if (packageHashs == null || packageHashs.size() != blockSize) {
            throw new InvalidFieldsException("Invalid packageHashs \"" + packageHashs + "\"");
        }
        if (nonce == null) {
            throw new InvalidFieldsException("Invalid nonce");
        }
        if (blockHash == null || blockHash.length != 32) {
            throw new InvalidFieldsException("Invalid blockHash");
        }
        BigInteger target = calcTarget();
        byte[] hash = calcBlockHash();
        BigInteger hashInteger = new BigInteger(hash);
        if (hashInteger.compareTo(target) == 1) {
            throw new InvalidFieldsException("BlockHash did not reach the target");
        }
        if (!Arrays.equals(blockHash, hash)) {
            throw new InvalidFieldsException("Corrupted block = " + blockHash);
        }*/
    }
}
