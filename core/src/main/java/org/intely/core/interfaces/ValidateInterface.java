package org.intely.core.interfaces;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import org.intely.core.exception.InvalidFieldsException;

public interface ValidateInterface {
    public static final DecimalFormat VALUE_FORMAT = new DecimalFormat("#.000000000000000");
    
    public void validate() throws InvalidFieldsException;
    
    default void validadeValue(String value, String name) throws InvalidFieldsException{
        if (value == null) {
            throw new InvalidFieldsException("Invalid "+name+" is NULL");
        }
        if (!value.matches("^[0-9]{1,10}(\\.[0-9]{0,15})?$") && !value.matches("^[0-9]{1,15}E-[0-9]{1,2}$")) {
            throw new InvalidFieldsException("Invalid value "+name+" \""+value+"\"");
        }
    }
    
    default void validadeValue(String value, String name, int maxScale, int maxPrecision) throws InvalidFieldsException{
        if (value == null) {
            throw new InvalidFieldsException("Invalid "+name+" is NULL");
        }
        if (!value.matches("^[0-9]{1,"+(maxPrecision-maxScale)+"}(\\.[0-9]{0,"+maxScale+"})?$")) {
            throw new InvalidFieldsException("Invalid value "+name+" \""+value+"\"");
        }
    }
    
    default byte[] formatValue(BigDecimal value){
        try {
            return VALUE_FORMAT.format(value).replace(",", ".").getBytes("ASCII");
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    default byte[] formatValue(Long value){
        try {
            return value.toString().replace(",", ".").getBytes("ASCII");
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    default byte[] formatValue(Integer value){
        try {
            return value.toString().replace(",", ".").getBytes("ASCII");
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    default byte[] formatValue(String value){
        try {
            return value.getBytes("ASCII");
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        }
    }
}
