package org.intely.core.domain.enums;

public enum ActivityType {
    UNKNOWN, CREDITED, DEBITED, MINNING_PACKAGE, MINNING_BLOCK;
}
