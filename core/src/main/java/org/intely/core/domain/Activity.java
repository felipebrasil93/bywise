package org.intely.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;
import lombok.*;
import org.intely.core.domain.enums.ActivityType;

@Getter
@Setter
@Entity
@NoArgsConstructor
public class Activity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private ActivityType type;
    private byte[] hashActivity;
    private BigDecimal valueActivity;
    private boolean inBlock;

    /*@ManyToOne
    @JoinColumn(name = "WALLET_ID")
    private Wallet wallet;

    @JsonIgnore
    @OneToOne(mappedBy = "activity", cascade = CascadeType.ALL)
    private Deposit deposit;

    @JsonIgnore
    @OneToOne(mappedBy = "activity", cascade = CascadeType.ALL)
    private Debit debit;

    @JsonIgnore
    @OneToOne(mappedBy = "activity", cascade = CascadeType.ALL)
    private Package pac;
    
    @JsonIgnore
    @OneToOne(mappedBy = "activity", cascade = CascadeType.ALL)
    private Block block;*/

    public Activity(ActivityType type, byte[] hashActivity) {
        this.type = ActivityType.UNKNOWN;
        this.hashActivity = hashActivity;
        this.inBlock = false;
    }
    
    
    
    public Activity(Deposit deposit) {
        //this.deposit = deposit;
        //this.wallet = deposit.getCreditedWallet();
        this.type = ActivityType.CREDITED;
        this.hashActivity = deposit.getHashActivity();
        this.valueActivity = deposit.getBalanceValue();
        this.inBlock = false;
    }

    public Activity(Debit debit, Deposit deposit) {
        //this.debit = debit;
        //this.wallet = debit.getDebitedWallet();
        this.type = ActivityType.DEBITED;
        this.hashActivity = deposit.getHashActivity();
        this.valueActivity = debit.getTxValue();
        this.inBlock = false;
    }

    public Activity(Block block, Wallet minner) {
        //this.block = block;
        //this.wallet = minner;
        this.type = ActivityType.MINNING_BLOCK;
        this.hashActivity = block.getBlockHash();
        this.valueActivity = block.getBlockReward();
        this.inBlock = true;
    }
    
    public Activity(Package pac, Block block, Wallet minner) {
        //this.pac = pac;
        //this.wallet = minner;
        this.type = ActivityType.MINNING_PACKAGE;
        //this.hashActivity = pac.getPackageHash();
        this.valueActivity = block.getPackageReward();
        this.inBlock = true;
    }
}
