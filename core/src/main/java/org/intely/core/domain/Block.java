package org.intely.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.*;
import lombok.*;

@NoArgsConstructor
@Getter
@Setter
@Entity
public class Block implements Serializable {

    private static final Integer LIMIT_PACKAGES = 31250;
    private static final BigDecimal LIMIT_AMOUNT = new BigDecimal("1000000000");
    private static final BigDecimal MINING_REWARD = new BigDecimal("30");
    private static final BigDecimal MINING_BLOCK_REWARD = new BigDecimal("20");
    private static final BigDecimal MINING_PACKAGE_REWARD = new BigDecimal("10");
    private static final BigDecimal FIRST_BLOCK = new BigDecimal("10000000");
    //TARGET_MAX = 0x00000FFFF0000000000000000000000000000000000000000000000000000000
    private static final BigDecimal TARGET_MAX = new BigDecimal("1110426256551982323683968927107989468512300641233199776096820318605148160");

    private static final long serialVersionUID = 1L;

    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private byte[] publicKey;
    private byte[] previousBlockHash;
    @Column(precision = 40, scale = 5)
    private BigDecimal difficultBlock;
    @Column(precision = 40, scale = 5)
    private BigDecimal difficultPackages;
    @Column(precision = 40, scale = 5)
    private BigDecimal difficult;
    private Long height;
    @Column(precision = 25, scale = 15)
    private BigDecimal coinsCreated;
    @Column(precision = 25, scale = 15)
    private BigDecimal totalFee;
    @Column(precision = 25, scale = 15)
    private BigDecimal reward;
    @Column(precision = 25, scale = 15)
    private BigDecimal blockReward;
    @Column(precision = 25, scale = 15)
    private BigDecimal packageReward;
    private Long created;
    private Integer blockSize;
    //private List<byte[]> packageHashs;
    private Long nonce;
    private byte[] blockHash;
    
    private Boolean isProcessed = false;
    private Boolean isValidated = false;
    private Boolean isMain = false;

    public boolean haveMiningReward(Long height, BigDecimal coinsCreated, BigDecimal totalFee, Integer blockSize) {
        return coinsCreated.add(calcMiningReward(height, coinsCreated, totalFee, blockSize)).compareTo(LIMIT_AMOUNT) == -1;
    }

    public BigInteger calcTarget() {
        //target = ((blockSize+1)/difficultPackages)*(TARGET_MAX/difficultBlock)
        return (new BigDecimal(blockSize + 1)).divide(difficultBlock).multiply(TARGET_MAX.divide(difficultBlock)).toBigInteger();
    }

    private static BigDecimal calcMiningReward(Long height, BigDecimal coinsCreated, BigDecimal totalFee, Integer blockSize) {
        if (height == 0L) {
            return FIRST_BLOCK;
        } else if (coinsCreated.add(MINING_REWARD).compareTo(LIMIT_AMOUNT) == -1) {
            if (blockSize == 0) {
                return totalFee.add(MINING_BLOCK_REWARD);
            } else {
                return totalFee.add(MINING_REWARD);
            }
        } else {
            return totalFee;
        }
    }

    private static BigDecimal calcPackageReward(Integer blockSize) {
        if (blockSize == 0) {
            return BigDecimal.ZERO;
        } else {
            return MINING_PACKAGE_REWARD.divideToIntegralValue(new BigDecimal(blockSize));
        }
    }

    private static BigDecimal calcBlockReward(BigDecimal reward, Integer blockSize) {
        return reward.subtract(calcPackageReward(blockSize).multiply(new BigDecimal(blockSize)));
    }

    public static Block generateBlock(byte[] publicKey, Block block) {
        Block b = new Block();
        b.publicKey = publicKey;
        b.previousBlockHash = block.blockHash;
        b.difficultBlock = block.difficultBlock;
        b.difficultPackages = block.difficultPackages;
        b.height = block.height + 1L;
        b.coinsCreated = BigDecimal.ZERO;
        b.totalFee = BigDecimal.ZERO;
        b.reward = calcMiningReward(b.height, b.coinsCreated, b.totalFee, 0);
        b.blockReward = calcBlockReward(b.reward, 0);
        b.packageReward = calcPackageReward(0);
        b.created = System.currentTimeMillis();
        b.blockSize = 0;
        b.nonce = null;
        b.blockHash = null;
        return b;
    }
    
    @Override
    public String toString() {
        return "Block{" + "id=" + id + ", publicKey=" + publicKey + ", previousBlockHash=" + previousBlockHash + ", difficultBlock=" + difficultBlock + ", difficultPackages=" + difficultPackages + ", height=" + height + ", coinsCreated=" + coinsCreated + ", totalFee=" + totalFee + ", reward=" + reward + ", blockReward=" + blockReward + ", packageReward=" + packageReward + ", created=" + created + ", blockSize=" + blockSize + ", nonce=" + nonce + ", blockHash=" + blockHash + ", isProcessed=" + isProcessed + ", isValidated=" + isValidated + ", isMain=" + isMain + '}';
    }
}
