package org.intely.core.domain;

import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.Getter;
import lombok.Setter;
import org.intely.core.configs.SetupConfigs;
import org.intely.core.domain.enums.NetworkEnum;
import org.intely.core.domain.enums.NodeTypeEnum;
import org.intely.core.dto.HandshakeDTO;
import org.intely.core.dto.HandshakeTokenDTO;
import org.intely.core.dto.InfoDTO;
import org.intely.core.dto.WalletHandshakeDTO;
import org.intely.core.exception.RateLimitException;
import org.intely.core.security.JWTUtil;

@Setter
@Getter
public class Node {

    private String name;
    private final String id;
    private final String address;
    private final Integer port;
    private NetworkEnum network;
    private Boolean connected;
    private Boolean disconnected = false;
    private NodeTypeEnum type;
    private String token;
    private Integer banPoints;
    private Long lastReceiverRequest;
    private Long lastMakeRequest = 0L;
    private Long expiration;
    private Long height;
    private Integer minDelay;
    private String version;
    private String software;

    public Node(String address, HandshakeDTO handshakeDTO) {
        this.id = address + ":" + handshakeDTO.getPort();
        this.name = handshakeDTO.getName();
        this.address = address;
        this.port = handshakeDTO.getPort();
        this.network = handshakeDTO.getNetwork();
        this.connected = false;
        this.banPoints = 0;
        this.type = handshakeDTO.getType();
        this.lastReceiverRequest = System.currentTimeMillis();
        this.minDelay = handshakeDTO.getMinDelay();
        this.version = handshakeDTO.getVersion();
        this.software = handshakeDTO.getSoftware();
        this.token = handshakeDTO.getYouToken();
        this.expiration = JWTUtil.getExpiration(this.token);
        this.height = handshakeDTO.getHeight();
    }
    
    public Node(String address, WalletHandshakeDTO handshakeDTO) {
        this.id = address + ":" + 0;
        this.name = "wallet-node";
        this.address = address;
        this.port = 0;
        this.network = handshakeDTO.getNetwork();
        this.connected = false;
        this.banPoints = 0;
        this.type = handshakeDTO.getType();
        this.lastReceiverRequest = System.currentTimeMillis();
        this.minDelay = 1000;
        this.version = handshakeDTO.getVersion();
        this.software = handshakeDTO.getSoftware();
        this.token = null;
        this.expiration = JWTUtil.getExpiration(this.token);
        this.height = 0L;
    }

    public Node(NetworkEnum network, String address, Integer port, String name) {
        this.id = address + ":" + port;
        this.name = name;
        this.address = address;
        this.port = port;
        this.network = network;
        this.type = NodeTypeEnum.MINNER;
        this.connected = false;
        this.banPoints = 0;
        this.lastReceiverRequest = System.currentTimeMillis();
        this.minDelay = SetupConfigs.getInstance().getMinDelay();
        this.version = SetupConfigs.VERSION;
        this.software = null;
        this.token = null;
        this.expiration = null;
    }

    public void updateNode(Node node) {
        this.minDelay = node.minDelay;
        this.version = node.version;
        this.software = node.software;
        if (node.token != null) {
            this.token = node.token;
        }
        if (this.token != null) {
            this.expiration = JWTUtil.getExpiration(this.token);
        }
    }

    public void updateNode(InfoDTO info) {
        this.minDelay = info.getMinDelay();
        this.version = info.getVersion();
        this.software = info.getSoftware();
        this.name = info.getName();
        this.network = info.getNetwork();
    }

    public void updateNode(HandshakeTokenDTO info) {
        this.minDelay = info.getMinDelay();
        this.version = info.getVersion();
        this.software = info.getSoftware();
        this.height = info.getHeight();
        this.name = info.getName();
        this.token = info.getToken();
        this.type = info.getType();
        if (this.token != null) {
            this.expiration = JWTUtil.getExpiration(this.token);
        }
    }

    public void receiverRequest() throws RateLimitException {
        if (isBan()) {
            throw new RateLimitException();
        } else {
            disconnected = false;
            long now = System.currentTimeMillis();
            if (now - lastReceiverRequest < SetupConfigs.getInstance().getMinDelay()) {
                banPoints++;
                lastReceiverRequest = now;
                throw new RateLimitException();
            } else {
                lastReceiverRequest = now;
            }
        }
    }

    public synchronized void makeRequest() {
        long now = System.currentTimeMillis();
        long diff = now - lastMakeRequest;
        if (diff < minDelay) {
            try {
                Thread.sleep(minDelay - diff);
            } catch (InterruptedException ex) {
                Logger.getLogger(Node.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        lastMakeRequest = System.currentTimeMillis();
    }

    public boolean isBan() {
        return banPoints >= SetupConfigs.getInstance().getLimitBanPoints();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Node other = (Node) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public String toString() {
        return "Node{" + "name=" + name + ", address=" + address + ", port=" + port + ", network=" + network + ", connected=" + connected + ", disconnected=" + disconnected + ", token=" + token + ", banPoints=" + banPoints + ", lastReceiverRequest=" + lastReceiverRequest + ", lastMakeRequest=" + lastMakeRequest + ", expiration=" + expiration + ", height=" + height + ", minDelay=" + minDelay + ", version=" + version + ", software=" + software + '}';
    }

}
