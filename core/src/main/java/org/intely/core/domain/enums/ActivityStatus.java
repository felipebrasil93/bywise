package org.intely.core.domain.enums;

public enum ActivityStatus {
    VALIDATED, IN_PACKAGE, IN_BLOCK;
}
