package org.intely.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.*;
import lombok.*;

@Data
@EqualsAndHashCode(exclude = "inputs")

@NoArgsConstructor
@Getter
@Setter
@Entity
public class Deposit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private byte[] creditedPublicKeyHash;
    @Column(precision = 25, scale = 15)
    private BigDecimal balanceValue;
    @Column(precision = 25, scale = 15)
    private BigDecimal feeValue;
    private Long created;
    @OneToMany(mappedBy = "deposit", cascade = CascadeType.ALL)
    private List<Debit> inputs;
    private byte[] hashActivity;
    
    @JsonIgnore
    @OneToOne(mappedBy = "deposit", cascade = CascadeType.ALL)
    private Wallet creditedWallet;
    /*@OneToOne
    @JsonIgnore
    @JoinColumn(name = "activity_id")
    private Activity activity;*/

}
