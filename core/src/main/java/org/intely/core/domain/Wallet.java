package org.intely.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Base64;
import java.util.List;
import javax.persistence.*;
import lombok.*;
import org.intely.core.helpers.Hash;

@NoArgsConstructor
@Getter
@Setter
@Entity
public class Wallet implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private byte[] publicKeyHash;
    @Column(precision = 40, scale = 5)
    private BigDecimal balance;
    private Long lastOperation;
    
    @OneToOne
    @JoinColumn(name = "deposit_id")
    private Deposit deposit;
    @OneToMany(mappedBy = "debitedWallet", cascade = CascadeType.ALL)
    private List<Debit> debits;

    public Wallet(Deposit deposit) {
        this.publicKeyHash = deposit.getCreditedPublicKeyHash();
        this.balance = BigDecimal.ZERO;
        this.lastOperation = 0L;
    }
    
    public Wallet(byte[] publicKey) {
        this.publicKeyHash = Hash.sha256(publicKey);
        this.balance = BigDecimal.ZERO;
        this.lastOperation = 0L;
    }
    
    public String sign(byte[] data){
        return "64x"+Base64.getEncoder().encodeToString(data);
    }
    
    public boolean validate(byte[] data, String signature){
        return ("64x"+Base64.getEncoder().encodeToString(data)).equals(signature);
    }
}
