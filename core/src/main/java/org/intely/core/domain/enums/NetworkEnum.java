package org.intely.core.domain.enums;

import lombok.Getter;

@Getter
public enum NetworkEnum {
    MAIN_NET(1, 5478, new String[]{"localhost:5478"}),
    TEST_NET(2, 8080, new String[]{"192.168.0.16:8080"});
    
    private final byte walletAddress;
    private final int defaultPorts;
    private final String[] defaultIPs;

    private NetworkEnum(int walletAddress, int defaultPorts, String[] defaultIPs) {
        this.walletAddress = (byte) walletAddress;
        this.defaultPorts = defaultPorts;
        this.defaultIPs = defaultIPs;
    }
}
