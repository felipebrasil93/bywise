package org.intely.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.*;
import lombok.*;

@NoArgsConstructor
@Getter
@Setter
@Entity
public class Package implements Serializable {

    private static final Integer LIMIT_ACTIVITIES = 65536;
    private static final long serialVersionUID = 1L;

    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private byte[] publicKeyHash;
    private byte[] previousPackageHash;
    @Column(precision = 25, scale = 15)
    private BigDecimal total;
    @Column(precision = 25, scale = 15)
    private BigDecimal totalFee;
    @Column(precision = 25, scale = 15)
    private BigDecimal reward;
    private Long created;
    private Integer packageSize;
    //@ManyToMany(mappedBy = "activities_packages", cascade = CascadeType.ALL)
    //private List<byte[]> activitiesHash;
    
    //private byte[] packageHash;

}
