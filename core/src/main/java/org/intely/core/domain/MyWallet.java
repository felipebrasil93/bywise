package org.intely.core.domain;

import lombok.*;
import org.intely.core.helpers.Hash;
import org.intely.core.helpers.WalletTools;

@Getter
public class MyWallet {

    private final byte[] publicKey;
    private final byte[] privateKey;
    private final byte[] publicKeyHash;
    private final String Address;

    public MyWallet(byte[] publicKey, byte[] privateKey) {
        this.publicKey = publicKey;
        this.privateKey = privateKey;
        this.Address = WalletTools.getAddress(publicKey);
        this.publicKeyHash = Hash.sha256(publicKey);
    }

}
