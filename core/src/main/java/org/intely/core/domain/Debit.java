package org.intely.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;
import lombok.*;

@NoArgsConstructor
@Getter
@Setter
@Entity
public class Debit implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private byte[] publicKey;
    @Column(precision=25, scale=15)
    private BigDecimal txValue;
    private Integer position;
    private byte[] signature;
    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "DEBITED_WALLET_ID")
    private Wallet debitedWallet;
    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "DEPOSIT_ID")
    private Deposit deposit;
    /*@OneToOne
    @JsonIgnore
    @JoinColumn(name = "activity_id")
    private Activity activity;*/
}
