package org.intely.core.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.intely.core.components.HttpRequest;
import org.intely.core.configs.SetupConfigs;
import org.intely.core.domain.Node;
import org.intely.core.domain.enums.NodeTypeEnum;
import org.intely.core.dto.HandshakeDTO;
import org.intely.core.dto.HandshakeTokenDTO;
import org.intely.core.dto.InfoDTO;
import org.intely.core.dto.NodeDTO;
import org.intely.core.exception.CantConnectException;
import org.intely.core.exception.DisconectedNodeException;
import org.intely.core.exception.InvalidFieldsException;
import org.intely.core.exception.WalletNodeException;
import org.intely.core.repositories.NodeRepository;
import org.intely.core.security.JWTUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NodeService {

    @Autowired
    private NodeRepository repository;
    @Autowired
    private BlockService blockService;
    @Autowired
    private HttpRequest http;
    @Autowired
    private JWTUtil jwtUtil;

    public void tryConnectNode(Node node) throws CantConnectException, WalletNodeException {
        HandshakeDTO myHand = new HandshakeDTO();
        myHand.setNetwork(SetupConfigs.getInstance().getNetwork());
        myHand.setTimestamp(System.currentTimeMillis());
        myHand.setName(SetupConfigs.getInstance().getName());
        myHand.setMinDelay(SetupConfigs.getInstance().getMinDelay());
        myHand.setPort(SetupConfigs.getInstance().getPort());
        myHand.setSoftware(SetupConfigs.SOFTWARE);
        myHand.setVersion(SetupConfigs.VERSION);
        myHand.setType(NodeTypeEnum.MINNER);
        myHand.setHeight(blockService.getHighestBlock());
        myHand.setYouToken(forceConnectNode(node));
        try {
            HandshakeTokenDTO token = http.post("/nodes/handshake", node, HttpRequest.GSON.toJson(myHand), HandshakeTokenDTO.class);
            token.validate();
            if (token.getSucess()) {
                node.updateNode(token);
            } else {
                repository.removeConnectedNode(node);
                throw new CantConnectException("Node " + node.getId() + " answered " + token.getMessage());
            }
        } catch (DisconectedNodeException | InvalidFieldsException ex) {
            throw new CantConnectException(ex.getMessage());
        }
    }

    public String forceConnectNode(Node node) throws CantConnectException {
        repository.connectNode(node);
        return jwtUtil.generateToken(node);
    }
    
    public void forceDisconnectNode(Node node) {
        repository.removeConnectedNode(node);
    }

    public InfoDTO generateMyInfoDTO() {
        InfoDTO info = new InfoDTO();
        info.setTimestamp(System.currentTimeMillis());
        info.setIsFull(repository.isFull());
        ArrayList<NodeDTO> nodes = new ArrayList<>();
        repository.getConnectedNodes().forEach(node -> {
            nodes.add(new NodeDTO(node));
        });
        info.setNodes(nodes);
        info.setMinDelay(SetupConfigs.getInstance().getMinDelay());
        info.setName(SetupConfigs.getInstance().getName());
        info.setNetwork(SetupConfigs.getInstance().getNetwork());
        info.setPort(SetupConfigs.getInstance().getPort());
        info.setSoftware(SetupConfigs.SOFTWARE);
        info.setVersion(SetupConfigs.VERSION);
        return info;
    }

    public void validateNodeConnectionWithToken(Node node) throws DisconectedNodeException, WalletNodeException {
        try {
            http.get("/nodes/validate", node);
        } catch (DisconectedNodeException ex) {
            throw new DisconectedNodeException("Disconnect because " + ex.getMessage());
        }
    }

    public Node getRandomNode() {
        Random rd = new Random();
        List<Node> nodes = repository.getNodes();
        if (nodes.isEmpty()) {
            nodes = Arrays.asList(SetupConfigs.initialKnownNodes());
        }
        Node randomNode = nodes.get(rd.nextInt(nodes.size()));
        return randomNode;
    }

    public void publish(String repo, Object dto) {
        repository.getConnectedNodes().forEach(node -> {
            try {
                http.post(repo, node, HttpRequest.GSON.toJson(dto));
            } catch (DisconectedNodeException ex) {
                Logger.getLogger(NodeService.class.getName()).log(Level.SEVERE, null, ex);
            } catch (WalletNodeException ex) {
            }
        });
    }

    public InfoDTO consultNodeInfo(Node node) throws DisconectedNodeException, WalletNodeException {
        try {
            InfoDTO info = http.get("/nodes/info", node, InfoDTO.class);
            info.validate();
            node.updateNode(info);
            info.getNodes().forEach(newNodeDTO -> {
                repository.addNode(newNodeDTO.toNode(info.getNetwork()));
            });
            return info;
        } catch (DisconectedNodeException | InvalidFieldsException ex) {
            throw new DisconectedNodeException("Disconnected " + node.getId() + " because " + ex.getMessage());
        }
    }
    
    public void pingNode(Node node) throws DisconectedNodeException, WalletNodeException {
        http.get("/nodes/ping/"+SetupConfigs.getInstance().getPort(), node);
    }

    public List<Node> getConnectedNodes() {
        return repository.getConnectedNodes();
    }
}
