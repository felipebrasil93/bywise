package org.intely.core.services;

import org.intely.core.domain.Node;
import org.intely.core.repositories.NodeRepository;
import org.intely.core.security.NodeSS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class NodeDetailsService implements UserDetailsService{
    
    @Autowired
    private NodeRepository repository;

    @Override
    public UserDetails loadUserByUsername(String id) throws UsernameNotFoundException {
        Node n = repository.getConectedNode(id);
        if(n == null) {
            throw new UsernameNotFoundException("Node not Found ("+id+")");
        }
        return new NodeSS(n);
    }
    
}
