package org.intely.core.services;

import org.intely.core.components.HttpRequest;
import org.intely.core.domain.Deposit;
import org.intely.core.repositories.DepositRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DepositService {

    @Autowired
    private DepositRepository depositRepository;
    @Autowired
    private NodeService nodeService;
    @Autowired
    private HttpRequest http;

    public void publishDeposit(Deposit deposit) {
        depositRepository.save(deposit);
        new Thread(){
            @Override
            public void run() {
                nodeService.publish("/deposits/add", deposit);
            }
        }.start();
    }

}
