package org.intely.core.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Hex;
import org.intely.core.components.HttpRequest;
import org.intely.core.domain.Block;
import org.intely.core.domain.Node;
import org.intely.core.domain.Wallet;
import org.intely.core.dto.BlockDTO;
import org.intely.core.exception.DisconectedNodeException;
import org.intely.core.exception.WalletNodeException;
import org.intely.core.repositories.BlockRepository;
import org.intely.core.repositories.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

@Service
public class BlockService {

    @Autowired
    private BlockRepository repository;
    @Autowired
    private NodeService nodeService;
    @Autowired
    private WalletRepository walletRepository;
    @Autowired
    private HttpRequest http;
    private Block zeroBlock;
    private Long highestBlockHeight;
    private Block highestBlock;
    private boolean modifiqued = true;
    private boolean sync = false;

    public synchronized Long getHighestBlock() {
        if (modifiqued) {
            modifiqued = false;
            highestBlock = repository.highestHeight();
            Long height;
            if (highestBlock == null) {
                height = 0L;
            } else {
                height = highestBlock.getHeight();
            }
            highestBlockHeight = height;

        }
        return highestBlockHeight;
    }

    public boolean isSync() {
        return sync;
    }

    public void updateHighestBlock() {
        modifiqued = true;
    }

    public void publishBlock(Block block) {
        repository.save(block);
        new Thread(){
            @Override
            public void run() {
                nodeService.publish("/blocks/add", block);
            }
        }.start();
    }

    public Block getBlockToBeMinned(byte[] publicKey) {
        Block highest = repository.highestHeight();
        if (highest == null) {
            Block blockZero = getBlockZero();
            highest = blockZero;
        }
        return Block.generateBlock(publicKey, highest);
    }

    public Block getNotProcessedBlock() {
        List<Block> blocks = repository.findBlockNotProcessed();
        if (blocks.isEmpty()) {
            return null;
        } else {
            Random rd = new Random();
            return blocks.get(rd.nextInt(blocks.size()));
        }
    }

    public Block getHighestBlockNotMain() {
        return repository.findHighestBlockNotMain();
    }

    public BlockDTO[] consultBlocksByHeight(long height, Node node) throws DisconectedNodeException, WalletNodeException {
        try {
            return http.get("/blocks/" + height, node, BlockDTO[].class);
        } catch (DisconectedNodeException ex) {
            throw new DisconectedNodeException("Disconnected " + node.getId() + " because " + ex.getMessage());
        }
    }

    public Block[] searchBlockchainByHash(byte[] blockHash, int size) throws DisconectedNodeException {
        for (Node node : nodeService.getConnectedNodes()) {
            try {
                BlockDTO[] blocksDTO = http.get("/blocks/byHash/" + Hex.encodeHexString(blockHash) + "/" + size, node, BlockDTO[].class);
                if (blocksDTO != null && blocksDTO.length != 0) {
                    Block[] blocks = new Block[blocksDTO.length];
                    for (int i = 0; i < blocksDTO.length; i++) {
                        blocks[i] = blocksDTO[i].toBlock();
                    }
                    return blocks;
                }
            } catch (DisconectedNodeException ex) {
                throw new DisconectedNodeException("Disconnected " + node.getId() + " because " + ex.getMessage());
            } catch (WalletNodeException ex) {
            }
        }
        return null;
    }

    public Block getBlockZero() {
        if (zeroBlock == null) {
            try {
                Resource resource = new ClassPathResource("zero.json");
                ObjectMapper objectMapper = new ObjectMapper();
                zeroBlock = objectMapper.readValue(resource.getInputStream(), Block.class);
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        return zeroBlock;
    }

    public void verifyBlockZero() {
        Block blockZero = getBlockZero();
        blockZero.setIsValidated(true);
        blockZero.setIsProcessed(true);
        blockZero.setIsMain(true);
        if (repository.findBlockByHash(blockZero.getBlockHash()) == null) {
            Wallet w = new Wallet(blockZero.getPublicKey());
            w.setBalance(blockZero.getBlockReward());
            walletRepository.save(w);
            repository.save(blockZero);
        }
    }
}
