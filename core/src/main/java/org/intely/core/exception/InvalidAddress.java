package org.intely.core.exception;

public class InvalidAddress extends Exception{

    public InvalidAddress() {
    }

    public InvalidAddress(String errorMessage) {
        super(errorMessage);
    }
}
