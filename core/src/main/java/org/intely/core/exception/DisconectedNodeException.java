package org.intely.core.exception;

public class DisconectedNodeException extends Exception{

    public DisconectedNodeException(String message) {
        super(message);
    }
}
