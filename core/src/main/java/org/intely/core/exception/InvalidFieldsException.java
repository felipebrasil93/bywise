package org.intely.core.exception;

public class InvalidFieldsException extends Exception{

    public InvalidFieldsException(String message) {
        super(message);
    }
}
