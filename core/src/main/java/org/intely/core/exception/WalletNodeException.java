package org.intely.core.exception;

public class WalletNodeException extends Exception{

    public WalletNodeException() {
        super("Cant request wallet node type");
    }
}
