package org.intely.core.exception;

public class CantConnectException extends Exception{

    public CantConnectException(String message) {
        super(message);
    }
}
