package org.intely.core.configs;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.File;
import java.security.SecureRandom;
import java.util.Random;
import org.intely.core.domain.enums.NetworkEnum;
import lombok.Getter;
import org.intely.core.domain.Node;
import org.intely.core.helpers.HelperFile;

@Getter
public final class SetupConfigs {

    private static SetupConfigs INSTANCE = null;
    private static final String CONFIG_FILE = "config.cfg";
    public static final String SOFTWARE = "IntelyCore1-1.0";
    public static final String VERSION = "1";

    public synchronized static SetupConfigs getInstance() {
        if (INSTANCE == null) {
            File config = new File(CONFIG_FILE);
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            if (!config.exists()) {
                HelperFile.saveFile(config, getInfo() + gson.toJson(new SetupConfigs()));
                System.out.println("Choose your settings in \"" + CONFIG_FILE + "\"");
                System.exit(0);
            } else {
                try {
                    String json = HelperFile.readFile(config, true);
                    if (json == null) {
                        System.err.println("Invalid \"" + CONFIG_FILE + "\" file");
                        System.exit(-1);
                    } else {
                        INSTANCE = gson.fromJson(json, SetupConfigs.class);
                        INSTANCE.generateSecretJWT();
                    }
                } catch (Exception ex) {
                    System.err.println("Invalid \"" + CONFIG_FILE + "\" file");
                    System.exit(-1);
                }
            }
        }
        return INSTANCE;
    }

    private final NetworkEnum network = NetworkEnum.MAIN_NET;
    private final Integer port = network.getDefaultPorts();
    private final Integer minDelay = 1000;
    private final Integer limitBanPoints = 100;
    private final Integer minConectedNodes = 10;
    private final Integer maxConectedNodes = 20;
    private final Integer pruningLimit = 5000;
    private final String name = "node-"+Math.abs((new Random()).nextLong());
    private final Boolean minerBlockEnable = true;
    private byte[] secretJWT;

    public void generateSecretJWT() {
        SecureRandom rd = new SecureRandom();
        secretJWT = new byte[200];
        rd.nextBytes(secretJWT);
    }
    
    public boolean isSuportedVersion(String version){
        return VERSION.equals(version);
    }

    private static String getInfo() {
        return "####################################\n"
                + "#\n"
                + "# Network can be MAIN_NET or TEST_NET\n"
                + "# Default Port MAIN_NET is " + NetworkEnum.MAIN_NET.getDefaultPorts() + "\n"
                + "# Default Port TEST_NET is " + NetworkEnum.TEST_NET.getDefaultPorts() + "\n"
                + "#\n"
                + "####################################\n";
    }
    
    public static Node[] initialKnownNodes(){
        Node[] ns = new Node[1];
        ns[0] = new Node(getInstance().network, "192.168.0.14", getInstance().network.getDefaultPorts(), "firstNode");
        return ns;
    }
}
