package org.intely.core.configs;

public class P2PConfigs {
    public static final int SEARCH_NEW_NODES = 10000;
    public static final int TOKEN_EXPIRATION = 600000;
    public static final int TOKEN_REFRESH = 540000;
    public static final int MAX_DELAY = 10000;
    public static final int RETRY_DELAY = 3000;
    public static final int RETRY_ERROR_DELAY = 3000;
}
