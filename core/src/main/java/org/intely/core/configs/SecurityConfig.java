package org.intely.core.configs;

import org.h2.server.web.WebServlet;
import org.intely.core.domain.enums.NetworkEnum;
import org.intely.core.security.JWTAuthorizationFilter;
import org.intely.core.security.JWTUtil;
import org.intely.core.services.NodeDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.cors.*;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String[] PUBLIC_MATCHERS = {
        "/h2-console/**",
        "/server/v1/**",
        "/server/v1/blocks/**",
        "/server/v1/nodes/ping/**",
        "/server/v1/nodes/info",
        "/server/v1/nodes/handshake",
        "/server/v1/nodes/wallet/handshake",
        "/"
    };
    
    @Autowired
    private NodeDetailsService userDetailsService;
    
    @Autowired
    private JWTUtil jwtUtil;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //if (SetupConfigs.getInstance().getNetwork().equals(NetworkEnum.TEST_NET)) {
            http.headers().frameOptions().sameOrigin();
            http.csrf().disable();
        //}
        http.cors();
        http.authorizeRequests()
                .antMatchers(PUBLIC_MATCHERS).permitAll()
                .anyRequest().authenticated();
        http.addFilter(new JWTAuthorizationFilter(jwtUtil, userDetailsService, authenticationManager()));
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }
    
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception{
        auth.userDetailsService(userDetailsService);
    }

    @Bean
    public ServletRegistrationBean h2servletRegistration() {
        ServletRegistrationBean registrationBean = new ServletRegistrationBean(new WebServlet());
        registrationBean.addUrlMappings("/h2-console/*");
        return registrationBean;
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/server/**", new CorsConfiguration().applyPermitDefaultValues());
        return source;
    }
}
