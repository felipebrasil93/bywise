package blockchain.components;

import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

public class Block implements Serializable, Cloneable{

    private static final Random RD = new Random();
    public static final int LIMIT = 3;

    public final int id;
    public final Color color;
    public final int idNode;
    public final int numBlock;
    public final Block lastBlock;
    private ArrayList<Transaction> transactions;

    public Block(int idNode, Block lastBlock) {
        this.id = RD.nextInt();
        this.color = new Color(RD.nextFloat(), RD.nextFloat(), RD.nextFloat());
        this.idNode = idNode;
        this.lastBlock = lastBlock;
        this.transactions = new ArrayList();
        if (lastBlock == null) {
            this.numBlock = 0;
        } else {
            this.numBlock = lastBlock.numBlock + 1;
        }
    }

    public void addTransaction(Transaction tx) throws FullException {
        if (transactions.size() < LIMIT) {
            boolean repeted = false;
            for (Transaction tx1 : transactions) {
                if (tx1.id == tx.id) {
                    repeted = true;
                }
            }
            if (repeted) {
                throw new FullException();
            } else {
                transactions.add(tx);
            }
        } else {
            throw new FullException();
        }
    }

    public boolean isFull() {
        return transactions.size() == LIMIT;
    }

    public ArrayList<Transaction> getTransactions() {
        return transactions;
    }

    @Override
    public Block clone() throws CloneNotSupportedException {
        Block blk = (Block) super.clone();
        blk.transactions = new ArrayList<>();
        for (Transaction tx : transactions) {
            blk.transactions.add(tx.clone());
        }
        return blk;
    }
    
}
