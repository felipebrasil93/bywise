package blockchain.components;

import java.io.Serializable;
import java.util.Random;

public class Transaction implements Serializable, Cloneable {
    private static final Random RD = new Random();
    
    public final int id;

    public Transaction() {
        this.id = RD.nextInt();
    }
    
    public void validation(){
        
    }

    @Override
    public Transaction clone() throws CloneNotSupportedException {
        return (Transaction) super.clone();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Transaction other = (Transaction) obj;
        return this.id == other.id;
    }
    
}
