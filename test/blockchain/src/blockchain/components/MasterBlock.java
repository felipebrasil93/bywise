package blockchain.components;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

public class MasterBlock implements Serializable, Cloneable{
    private static final Random RD = new Random();
    private static final int LIMIT = 3;
    
    private final int id;
    private ArrayList<Block> blocks;

    public MasterBlock() {
        this.id = RD.nextInt();
        this.blocks = new ArrayList<>();
    }
    
    public void addBlock(Block blk) throws FullException{
        if (blocks.size() < LIMIT) {
            boolean repeted = false;
            for (Block blk1 : blocks) {
                if (blk1.id == blk.id) {
                    repeted = true;
                }
            }
            if (repeted) {
                throw new FullException();
            } else {
                blocks.add(blk);
            }
        } else {
            throw new FullException();
        }
    }
    
    @Override
    public MasterBlock clone() throws CloneNotSupportedException {
        MasterBlock mblk = (MasterBlock) super.clone();
        mblk.blocks = new ArrayList<>();
        for (Block blk : blocks) {
            mblk.blocks.add(blk.clone());
        }
        return mblk;
    }
}
