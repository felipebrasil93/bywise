package blockchain.net;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

public class Element {

    private static final Random RD = new Random();
    private final int radius;
    public float x, y;
    private final Color color;

    public Element(int x, int y, int radius) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.color = new Color(RD.nextFloat(), RD.nextFloat(), RD.nextFloat());
    }

    public void draw(Graphics g, Color color, float w, float h) {
        int posX = (int) x;
        int posY = (int) y;
        g.setColor(color);
        g.fillOval(posX - radius, posY - radius, radius * 2, radius * 2);
    }
    
    public void draw(Graphics g, float w, float h) {
        int posX = (int) x;
        int posY = (int) y;
        g.setColor(color);
        g.fillOval(posX - radius, posY - radius, radius * 2, radius * 2);
    }

    public void colide(Element e) {
        if (!e.equals(this)) {
            double modX = x - e.x;
            double modY = y - e.y;
            double mod = Math.sqrt(modX * modX + modY * modY);
            if (mod < radius * 6) {
                double dx = Math.sin(modX / mod) * 10;
                double dy = Math.sin(modY / mod) * 10;
                x += dx;
                y += dy;
            }
        }
    }

    public void toCenter(float mX, float mY) {
        double modX = x - mX;
        double modY = y - mY;
        double mod = Math.sqrt(modX * modX + modY * modY);
        if (mod > radius * 4) {
            double dx = Math.sin(modX / mod) * 10;
            double dy = Math.sin(modY / mod) * 10;
            x += -dx;
            y += -dy;
        }
    }

    public Color getColor() {
        return color;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }
}
