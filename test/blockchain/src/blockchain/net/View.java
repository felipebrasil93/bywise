package blockchain.net;

import blockchain.components.FullException;
import blockchain.components.Transaction;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class View extends JFrame {

    private static final Random RD = new Random();
    private static final int INIT_TIME = 10000;
    private static final int W = 1200;
    private static final int W_NODES = 2 * W / 3;
    private static final int W_BLOCKS = W - W_NODES;
    private static final int H = 600;
    private ArrayList<Node> nodes = new ArrayList<>();
    private Label l = new Label("asdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdf");

    public View() {
        super("Debug View");
        DrawPane dp = new DrawPane();
        setContentPane(dp);
        add(l);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(W, H);
        setVisible(true);
        setLocationRelativeTo(null);
        init();
        addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                System.out.println(".mouseClicked()");
                nodes.get(RD.nextInt(nodes.size())).receiverRequest(new Request("Transaction", new Transaction()));
            }
        });
    }

    public void init() {
        nodes = new ArrayList<>();
        for (int i = 0; i < 40; i++) {
            Node n = new Node(i);
            n.setX(W_NODES / 2 + RD.nextFloat() * 2 - 1);
            n.setY(H / 2 + RD.nextFloat() * 2 - 1);
            nodes.add(n);
            if (Node.mainNode == null) {
                Node.mainNode = n;
            }
        }

        new Thread() {
            @Override
            public void run() {
                long uptime = System.currentTimeMillis();
                do {
                    boolean init = System.currentTimeMillis() - uptime < INIT_TIME;
                    View.this.repaint();
                    for (int i = 0; i < nodes.size(); i++) {
                        Node n1 = nodes.get(i);
                        n1.update(init);
                        colideNodes(i, init, n1);
                    }
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(View.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } while (View.this.isVisible());
            }

            public void colideNodes(int i, boolean init, Node n1) {
                for (int j = 0; j < nodes.size() && init; j++) {
                    Node.mainNode.setX(W_NODES / 2);
                    Node.mainNode.setY(H / 2);
                    Node n2 = nodes.get(j);
                    n1.colide(n2);
                }
            }
        }.start();
    }

    class DrawPane extends JPanel {

        @Override
        public void paintComponent(Graphics g) {
            g.setColor(Color.BLACK);
            g.fillRect(W_NODES - 1, 0, 2, H);
            nodes.forEach((node) -> {
                node.draw(g, W_NODES, H);
            });
        }
        
        
    }

}
