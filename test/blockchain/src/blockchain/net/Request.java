package blockchain.net;

import blockchain.components.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Request {

    private static final int DELAY = 20;
    private int time = DELAY;
    private final String requestType;
    private final Object obj;

    public Request(String requestType, Transaction obj) {
        this.requestType = requestType;
        Object objTmp = null;
        try {
            objTmp = obj.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(Request.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.obj = objTmp;
    }

    public Request(String requestType, Block obj) {
        this.requestType = requestType;
        Object objTmp = null;
        try {
            objTmp = obj.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(Request.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.obj = objTmp;
    }

    public Request(String requestType, MasterBlock obj) {
        this.requestType = requestType;
        Object objTmp = null;
        try {
            objTmp = obj.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(Request.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.obj = objTmp;
    }

    public String getRequestType() {
        return requestType;
    }

    public Object getObj() {
        return obj;
    }

    public void update() {
        time--;
    }

    public boolean checkin() {
        return time <= 0;
    }
}
