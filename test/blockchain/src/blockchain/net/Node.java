package blockchain.net;

import blockchain.components.*;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Node extends Element {

    public static Node mainNode;
    private static final Random RD = new Random();
    private static final int LIMIT = 3;

    private final int id;
    private final ArrayList<Node> nodes;
    private final ArrayList<Request> requests;
    private final ArrayList<Transaction> txsMempool;
    private final ArrayList<Transaction> txs;
    private final ArrayList<Block> blks;
    private final ArrayList<MasterBlock> mblks;

    private Block lastBlock;

    public Node(int id) {
        super(0, 0, 15);
        this.id = id;
        this.nodes = new ArrayList();
        this.requests = new ArrayList();
        this.txsMempool = new ArrayList();
        this.txs = new ArrayList();
        this.blks = new ArrayList();
        this.mblks = new ArrayList();
        this.lastBlock = null;
    }

    public boolean conect(ArrayList<Node> previousNode, Node node) {
        try {
            addNode(node);
            return true;
        } catch (FullException ex) {
        }
        previousNode.add(node);
        for (int i = 0; i < 10; i++) {
            if (node.nodes.size() > 0) {
                Node n = node.nodes.get(RD.nextInt(node.nodes.size()));
                boolean jaVerificado = false;
                for (Node pn : previousNode) {
                    if (pn.equals(n)) {
                        jaVerificado = true;
                    }
                }
                if (!jaVerificado) {
                    if (conect(previousNode, n)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void update(boolean init) {
        if (nodes.size() < LIMIT && init) {
            conect(new ArrayList(), mainNode);
        } else {
            updateRequests();
            ArrayList<Request> req = getRequests();
            for (Request r : req) {
                switch (r.getRequestType()) {
                    case "Transaction":
                        Transaction tx0 = (Transaction) r.getObj();
                        boolean repeted = isRepeted(tx0);
                        if (!repeted) {
                            addNewTransaction(tx0);
                        }
                        break;
                    case "Block":
                        Block blk = (Block) r.getObj();
                        addBlock(blk);
                        break;
                    case "MasterBlock":
                        break;
                }
            }
            if (txs.size() >= Block.LIMIT) {
                Block blk = new Block(id, this.lastBlock);
                for (int i = txs.size() - 1; i >= 0; i--) {
                    try {
                        blk.addTransaction(txs.get(i));
                        txs.remove(i);
                    } catch (FullException ex) {
                    }
                }
                addBlock(blk);
            }
        }
    }

    private void addBlock(Block blk) {
        if (lastBlock != null) {
            if (lastBlock.numBlock == blk.numBlock) {
                if (blk.id < lastBlock.id) {
                    newBlock(blk);
                }
            } else if (lastBlock.numBlock < blk.numBlock) {
                newBlock(blk);
            }
        } else {
            newBlock(blk);
        }
    }

    private void newBlock(Block blk) {
        lastBlock = blk;
        for (Transaction tx0 : blk.getTransactions()) {
            Transaction repeted = null;
            for (Transaction tx1 : txs) {
                if(tx1.equals(tx0)){
                    repeted = tx1;
                }
            }
            if(repeted != null){
                txs.remove(repeted);
            }
        }
        sendRequest(new Request("Block", lastBlock));
    }

    private void addNewTransaction(Transaction tx0) {
        tx0.validation();
        txsMempool.add(tx0);
        sendRequest(new Request("Transaction", tx0));
    }

    private boolean isRepetedMempool(Transaction tx0) {
        boolean repeted = false;
        for (Transaction tx1 : txsMempool) {
            if (tx1.id == tx0.id) {
                repeted = true;
            }
        }
        return repeted;
    }
    
    private boolean isRepeted(Transaction tx0) {
        boolean repeted = false;
        for (Transaction tx1 : txs) {
            if (tx1.id == tx0.id) {
                repeted = true;
            }
        }
        return repeted;
    }

    private void updateRequests() {
        requests.forEach(r -> {
            r.update();
        });
    }

    private void addNode(Node node) throws FullException {
        if (nodes.size() < LIMIT && node.nodes.size() < LIMIT && !node.equals(this)) {
            boolean repeted = false;
            for (Node n : nodes) {
                if (n.id == node.id) {
                    repeted = true;
                }
            }
            if (repeted) {
                throw new FullException();
            } else {
                node.nodes.add(this);
                nodes.add(node);
                System.out.println("node " + id + " add node " + node.id);
            }
        } else {
            throw new FullException();
        }
    }

    @Override
    public void draw(Graphics g, float w, float h) {
        if (this.lastBlock != null) {
            super.draw(g, this.lastBlock.color, w, h);
        } else {
            super.draw(g, Color.LIGHT_GRAY, w, h);
        }
        nodes.forEach((node) -> {
            g.drawLine((int) x, (int) y, (int) node.x, (int) node.y);
        });
        g.setColor(Color.BLACK);
        g.setFont(new Font("Arial", Font.BOLD, 20));
        g.drawString(txsMempool.size() + "", (int) x-10, (int) y-10);
    }

    @Override
    public void colide(Element e) {
        super.colide(e);
        if (e.getClass().equals(Node.class)) {
            Node n = (Node) e;
            boolean isChild = false;
            for (Node node : nodes) {
                if (node.equals(n)) {
                    isChild = true;
                }
            }
            if (isChild) {
                toCenter(n.x, n.y);
            }
        }
    }

    private ArrayList<Request> getRequests() {
        ArrayList<Request> rs = new ArrayList();
        for (int i = requests.size() - 1; i >= 0; i--) {
            if (requests.get(i).checkin()) {
                rs.add(requests.remove(i));
            }
        }
        return rs;
    }

    public void receiverRequest(Request r) {
        requests.add(r);
    }

    private void sendRequest(Request r) {
        nodes.forEach(n -> {
            n.receiverRequest(r);
        });
    }

}
