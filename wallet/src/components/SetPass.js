import React, { Component } from 'react';
import { MDBAlert, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter, MDBInput } from 'mdbreact';

class SetPass extends Component {

    constructor(props) {
        super(props);
        this.state = {
            pass: '',
            passConfirm: '',
            accept: false,
            error: ''
        }
        this.updateForm = this.updateForm.bind(this);
        this.updateAccept = this.updateAccept.bind(this);
        this.save = this.save.bind(this);
    }

    updateForm(event) {
        let state = this.state;
        state[event.target.name] = event.target.value;
        this.setState(state);
        this.validate();
    }

    updateAccept() {
        if(this.state.accept){
            this.setState({ accept: false, error: 'Save your password in a safe place' });
        } else {
            this.setState({ accept: true, error: '' });
        }
    }

    validate() {
        if (this.state.pass.length < 6) {
            this.setState({ error: 'Password too short' });
        } else if (this.state.pass !== this.state.passConfirm) {
            this.setState({ error: 'Password do not match' });
        } else if (!this.state.accept) {
            this.setState({ error: 'Save your password in a safe place' });
        } else {
            this.setState({ error: '' });
            return true;
        }
        return false;
    }

    save() {
        if (this.validate()) {
            this.props.savePass(this.state.pass);
            this.props.toggle();
        }
    }

    render() {
        return (
            <MDBModal isOpen={this.props.active} toggle={this.props.toggle}>
                <MDBModalHeader toggle={this.props.toggle}>Set your password</MDBModalHeader>
                <MDBModalBody>
                    <MDBAlert color="warning">
                        <h4 className="alert-heading">Attention!</h4>
                        <p> This password is unrecoverable. If you lose it, you will lose access to your wallet and your coins. Write your password on paper and save it.</p>
                    </MDBAlert>
                    <p>The password will be used to encrypt your wallet and to confirm transactions.</p>
                    <MDBInput label="Write password" type="password" name="pass" onChange={this.updateForm} value={this.state.pass} />
                    <MDBInput label="Confirm password" type="password" name="passConfirm" onChange={this.updateForm} value={this.state.passConfirm} />
                    <div className="custom-control custom-checkbox mb-3" onClick={this.updateAccept}>
                        <input className="custom-control-input" type="checkbox" name="accept" onChange={() => {}} checked={this.state.accept} />
                        <label className="custom-control-label" htmlFor="customControlValidation1">I saved my password in a safe place</label>
                    </div>
                    <p className="red-text" style={{ display: this.state.error.length !== 0 ? 'block' : 'none' }}>
                        {this.state.error}
                    </p>
                </MDBModalBody>
                <MDBModalFooter>
                    <MDBBtn color="secondary" onClick={() => this.props.toggle()}>Cancel</MDBBtn>
                    <MDBBtn color="primary" onClick={this.save}>Save changes</MDBBtn>
                </MDBModalFooter>
            </MDBModal>
        );
    }
}

export default SetPass;