import React, { Component } from 'react';
import {
    MDBNav, MDBNavItem, MDBNavLink
} from "mdbreact";
import Network from '../domains/Network';

class NavBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            connectedNode: Network.connectedNode,
            active: 0
        }
    }

    componentDidMount() {
        this.updateConnectedState = setInterval(() => {
            this.setState({ connectedNode: Network.connectedNode });
        }, 1000);
        this.refreshTimer = setInterval(() => {
            Network.refreshToken();
        }, 580000);
    }

    componentWillUnmount() {
        if (this.updateConnectedState !== null) {
            clearInterval(this.updateConnectedState);
            this.updateConnectedState = null;
        }
    }

    render() {
        return (
            <MDBNav className="nav-pills p-1 nav-justified" color="indigo">
                <MDBNavItem>
                    <MDBNavLink className="white-text" exact to="/">Wallet</MDBNavLink>
                </MDBNavItem>
                <MDBNavItem>
                    <MDBNavLink className="white-text" to="/send">Send</MDBNavLink>
                </MDBNavItem>
                <MDBNavItem>
                    <MDBNavLink className="white-text" to="/receiver">Receiver</MDBNavLink>
                </MDBNavItem>
                <MDBNavItem>
                    <MDBNavLink className="white-text" to="/nodes">{this.state.connectedNode === null ? 'Offline ' : 'Online'}</MDBNavLink>
                </MDBNavItem>
            </MDBNav>
        );
    }
}
export default NavBar;