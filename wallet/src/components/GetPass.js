import React, { Component } from 'react';
import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter, MDBInput } from 'mdbreact';

class GetPass extends Component {

    constructor(props) {
        super(props);
        this.state = {
            pass: '',
            error: ''
        }
        this.updateForm = this.updateForm.bind(this);
        this.save = this.save.bind(this);
    }

    updateForm(event) {
        let state = this.state;
        state[event.target.name] = event.target.value;
        this.setState(state);
        this.validate();
    }

    validate() {
        if (this.state.pass.length < 6) {
            this.setState({ error: 'Password too short' });
        } else {
            this.setState({ error: '' });
            return true;
        }
        return false;
    }

    save() {
        this.props.getPass(this.state.pass);
        this.props.toggle();
    }

    render() {
        return (
            <MDBModal isOpen={this.props.active} toggle={this.props.toggle}>
                <MDBModalHeader toggle={this.props.toggle}>Write your password</MDBModalHeader>
                <MDBModalBody>
                    <MDBInput label="Password" type="password" name="pass" onChange={this.updateForm} value={this.state.pass} />
                    <p className="red-text" style={{ display: this.state.error.length !== 0 ? 'block' : 'none' }}>
                        {this.state.error}
                    </p>
                </MDBModalBody>
                <MDBModalFooter>
                    <MDBBtn color="secondary" onClick={() => this.props.toggle()}>Cancel</MDBBtn>
                    <MDBBtn color="primary" onClick={this.save}>Ok</MDBBtn>
                </MDBModalFooter>
            </MDBModal>
        );
    }
}

export default GetPass;