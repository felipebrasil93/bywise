import React, { Component } from 'react';
import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';

class Message extends Component {

    render() {
        return (
            <MDBModal isOpen={this.props.active} toggle={this.props.toggle}
                className={"modal-notify white-text "+this.props.color}>
                <MDBModalHeader className="text-center" titleClass="w-100 font-weight-bold"toggle={this.props.toggle}>{this.props.title}</MDBModalHeader>
                <MDBModalBody className="text-center">{this.props.message}</MDBModalBody>
                <MDBModalFooter>
                    <MDBBtn color="primary" onClick={() => this.props.toggle()}>Ok</MDBBtn>
                </MDBModalFooter>
            </MDBModal>
        );
    }
}

export default Message;