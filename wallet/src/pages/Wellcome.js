import React, { Component } from 'react';
import Wallet from '../domains/Wallet';
import CryptoJS from 'crypto-js';
import download from 'downloadjs';
import { MDBBtn, MDBCol, MDBContainer, MDBRow } from "mdbreact";
import logo from "../logo.png";
import SetPass from '../components/SetPass';
import GetPass from '../components/GetPass';
import WalletTools from '../domains/WalletTools';

class Wellcome extends Component {

    constructor(props) {
        super(props);
        this.state = {
            progress: 0,
            run: false,
            showSetPass: false,
            showGetPass: false,
            download: false
        }
        this.wallets = [];
        this.uploadWallet = this.uploadWallet.bind(this);
        this._inputFile = React.createRef();
        this._homeLink = React.createRef();
    }

    componentWillUnmount() {
        if (this.createWallets !== null) {
            clearInterval(this.createWallets);
            this.createWallets = null;
        }
    }

    createWallet() {
        if (!this.state.run) {
            this.setState({ run: true, progress: 0 });
            this.wallets = [];
            this.createWallets = setInterval(() => {
                let i = this.state.progress + 1;
                if (i < 100) {
                    let w = new Wallet();
                    let data = {
                        privateKey: w.getPrivateKeyHex(),
                        publicKey: w.getPublicKeyHex(),
                        publicKeyHash: w.getPublicKeyHash()
                    }
                    this.wallets.push(data);
                    this.setState({ progress: i });
                } else {
                    clearInterval(this.createWallets);
                    this.setState({ run: false, progress: 100, showSetPass: true, download: true });
                }
            }, 500);
        }
    }

    uploadWallet(event) {
        var file = event.target.files[0];
        var reader = new FileReader();
        reader.onload = (event) => {
            try {
                this.wallets = JSON.parse(event.target.result);
                if (Array.isArray(this.wallets)) {
                    let start = "ok-";
                    if (this.wallets[0].privateKey.substring(0, start.length) === start) {
                        this.wallets.forEach(w => {
                            w.privateKey = w.privateKey.substring(start.length);
                        });
                        this.setState({ showSetPass: true, download: false });
                    } else {
                        this.toggleGetPass();
                    }
                } else {
                    alert("Invalid wallet");
                }
            } catch (e) {
                alert("Corrupted file");
            }
        };
        reader.readAsText(file);
    }

    setPass(pass) {
        this.wallets.forEach(w => {
            w.privateKey = CryptoJS.AES.encrypt("ok-" + w.privateKey, pass).toString();
        });
        if(this.state.download){
            download(JSON.stringify(this.wallets), "wallets.json", "text/plain");
        }
        this.wallets.forEach(w => {
            w.balance = 0;
            w.lastUse = 0;
        });
        WalletTools.saveWallets(this.wallets);
        this._homeLink.current.click();
    }

    getPass(pass) {
        try {
            var bytes = CryptoJS.AES.decrypt(this.wallets[0].privateKey, pass);
            var privateKeyHex = bytes.toString(CryptoJS.enc.Utf8);
            let start = "ok-";
            if (privateKeyHex.substring(0, start.length) === start) {
                this.wallets.forEach(w => {
                    w.balance = 0;
                    w.lastUse = 0;
                });
                WalletTools.saveWallets(this.wallets);
                this._homeLink.current.click();
            } else {
                alert("Wrong password");
            }
        } catch (e) {
            alert("Wrong password");
        }
    }

    toggleSetPass = () => {
        this.setState({
            showSetPass: !this.state.showSetPass
        });
    }

    toggleGetPass = () => {
        this.setState({
            showGetPass: !this.state.showGetPass
        });
    }

    render() {
        return (
            <MDBContainer>
                {
                    this.state.run ? <MDBRow center style={{ height: "80vh" }}>
                        <MDBCol middle="true" sm="8" className="text-center">
                            <div className="spinner-border text-primary" role="status" style={{ width: "5rem", height: "5rem" }}>
                                <span className="sr-only">Loading...</span>
                            </div>
                            <h1>Processing {this.state.progress}%</h1>
                        </MDBCol>
                    </MDBRow> : <MDBRow center style={{ height: "80vh" }}>
                            <MDBCol middle="true" sm="8" className="text-center">
                                <img src={logo} alt="logo" style={{ width: "10rem" }} />
                                <h1>Welcome to Hidracoin</h1>
                                <p className="mb-2">Create your wallet or upload it</p>
                                <MDBBtn color="light-blue" onClick={() => this.createWallet()}><strong>Create new wallet</strong></MDBBtn>
                                <MDBBtn color="light-blue" onClick={() => this._inputFile.current.click()}><strong>Upload your wallet</strong></MDBBtn>
                                <input ref={this._inputFile} type="file" accept=".json" onChange={this.uploadWallet} name="myFile" style={{ display: 'none' }} />
                            </MDBCol>
                        </MDBRow>
                }
                <SetPass toggle={this.toggleSetPass} active={this.state.showSetPass} savePass={(pass) => { this.setPass(pass) }} />
                <GetPass toggle={this.toggleGetPass} active={this.state.showGetPass} getPass={(pass) => { this.getPass(pass) }} />
                <a ref={this._homeLink} href="/" style={{ display: 'none' }}>link</a>
            </MDBContainer>
        );
    }
}
export default Wellcome;