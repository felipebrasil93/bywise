import React, { Component } from 'react';
import Network from '../domains/Network';
import { MDBContainer } from 'mdbreact';

const Node = (props) => {
    return (
        <div>
            Address: {props.node.host+":"+props.node.port}<br />
            Name: {props.node.name}<br />
            Software: {props.node.software}<br />
            Version: {props.node.version}<br />
            Last Block: {props.node.block}<br />
        </div>
    );
}

class Nodes extends Component {

    constructor(props) {
        super(props);
        this.state = {
            connectedNode: Network.connectedNode
        }
    }

    componentDidMount() {
        this.updateConnectedState = setInterval(() => {
            if (this.state.connectedNode !== Network.connectedNode) {
                this.setState({ connectedNode: Network.connectedNode });
            }
        }, 1000);
    }

    componentWillUnmount() {
        if (this.updateConnectedState !== null) {
            clearInterval(this.updateConnectedState);
            this.updateConnectedState = null;
        }
    }

    render() {
        return (
            <MDBContainer style={{ height: "80vh" }}>
                {
                    this.state.connectedNode != null ? <Node node={this.state.connectedNode}/> : ''
                }
                <button onClick={() => Network.request('/nodes/validate', {}, 'get', null)}>Connect</button>
            </MDBContainer>
        );
    }
}
export default Nodes;