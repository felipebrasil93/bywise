import React, { Component } from 'react';
import WalletTools from '../domains/WalletTools';
import { MDBBtn, MDBContainer } from "mdbreact";

class Receiver extends Component {

    constructor(props) {
        super(props);
        this.state = {
            addr: ''
        }
        this.getAddress = this.getAddress.bind(this);
    }

    getAddress() {
        let wallets = WalletTools.getWallets();
        let wallet = null;
        wallets.forEach(w => {
            if (w.lastUse === 0 && wallet === null) {
                wallet = w;
                w.lastUse = 1;
            }
        });
        if (wallet === null) {
            this.setState({ addr: 'Update your balance before' });
        } else {
            WalletTools.saveWallets(wallets);
            this.setState({ addr: wallet.publicKeyHash });
        }
    }

    render() {
        return (
            <MDBContainer style={{ height: "80vh" }}>
                <MDBBtn color="primary" onClick={this.getAddress}>Get Address</MDBBtn>
                <div className="text-center p-5 ow">
                    <h1>{this.state.addr}</h1>
                </div>
            </MDBContainer>
        );
    }
}
export default Receiver;