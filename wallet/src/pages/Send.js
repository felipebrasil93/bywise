import React, { Component } from 'react';
import WalletTools from '../domains/WalletTools';
import bigDecimal from 'js-big-decimal';
import CryptoJS from 'crypto-js';
import { MDBBtn, MDBContainer, MDBInput } from "mdbreact";
import Message from '../components/Message';
import Deposit from '../domains/Deposit';
import Wallet from '../domains/Wallet';
import Network from '../domains/Network';

class Send extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: '1',
            addr: 'dc552395981e2d5defc4e90635b026429f90437c4f5249a15d63a5aeb660725f',
            fee: '0.001',
            pass: '123456',
            total: '1.001',
            colorMessage: '',
            message: '',
            titleMessage: '',
            showMessage: false
        }
        this.balance = WalletTools.getBalance();
        this.send = this.send.bind(this);
        this.updateForm = this.updateForm.bind(this);
    }

    updateForm(event) {
        let state = this.state;
        try {
            state[event.target.name] = event.target.value;
            let v = new bigDecimal(state.value);
            let f = new bigDecimal('0.001');
            state.fee = v.multiply(f).getValue();
            state.total = v.add(v.multiply(f)).getValue();
            this.setState(state);
        } catch {
        }
    }

    toggle = () => {
        this.setState({
            showMessage: !this.state.showMessage
        });
    }

    showMessage(message, title, type) {
        this.setState({ message: message, titleMessage: title, colorMessage: type, showMessage: true });
    }

    send() {
        if (this.state.addr.length === 0) {
            this.showMessage("Invalid Address", 'Error', 'modal-warning');
        } else if (this.state.value.length === 0) {
            this.showMessage("Invalid Value", 'Error', 'modal-warning');
        } else if (parseFloat(this.state.total > parseFloat(this.balance))) {
            this.showMessage("Insufficient funds", 'Error', 'modal-warning');
        } else {
            let valueTransaction = new bigDecimal(this.state.total);
            let wallets = WalletTools.getWallets();
            let selectedWallets = [];
            let total = new bigDecimal("0");
            wallets.forEach(w => {
                if (w.balance > 0 && total.compareTo(valueTransaction) === -1) {
                    selectedWallets.push(w);
                    total.add(new bigDecimal(w.balance));
                }
            });

            let deposit = new Deposit(WalletTools.hexToBase64(this.state.addr), this.state.value, this.state.fee);
            total = new bigDecimal("0");
            selectedWallets.forEach(w => {

                var privateKeyHex = CryptoJS.AES.decrypt(w.privateKey, this.state.pass).toString(CryptoJS.enc.Utf8);
                let start = "ok-";
                if (privateKeyHex.substring(0, start.length) === start) {
                    let wallet = new Wallet();
                    wallet.importKey(privateKeyHex.substring(start.length));
                    deposit.addInput(wallet, this.state.total);
                } else {
                    deposit = null;
                    this.showMessage("Wrong password", 'Error', 'modal-error');
                    return;
                }
            });
            if (deposit != null) {
                console.log(JSON.stringify(deposit));
                Network.request('/deposits', deposit, 'post', null);
                this.showMessage("Submitted successfully", 'Success', 'modal-success');
                this.setState({ value: '', fee: '0', addr: '' });
            }
        }
    }

    render() {
        return (
            <MDBContainer style={{ height: "80vh" }}>
                <MDBInput label="Address" name="addr" value={this.state.addr}
                    onChange={this.updateForm} size="sm" icon="wallet" />
                <div className="font-italic">Your balance {this.balance} HDR</div>
                <MDBInput label="Value" name="value" value={this.state.value}
                    onChange={this.updateForm} size="sm" icon="money-bill-wave-alt" />
                <MDBInput label="Fee" name="fee" value={this.state.fee}
                    disabled size="sm" icon="hand-holding-usd" />
                <MDBInput label="Pass" name="pass" value={this.state.pass}
                    onChange={this.updateForm} size="sm" type="password" icon="lock" />
                <div className="font-italic">Total {this.state.total} HDR</div>
                <MDBBtn color="primary" onClick={this.send}>Send Value</MDBBtn>
                <Message toggle={this.toggle} active={this.state.showMessage} message={this.state.message} title={this.state.titleMessage} color={this.state.colorMessage} />
            </MDBContainer>
        );
    }
}
export default Send;