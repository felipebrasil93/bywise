import React, { Component } from 'react';
import { MDBContainer, MDBAlert, MDBBadge, MDBInput } from 'mdbreact';


class Debug extends Component {
  state = {
    modal: false
  }

  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  }

  render() {
    return (
      <MDBContainer>
        <div>
          <div header="Additional content">
            <MDBAlert color="warning">
              <h4 className="alert-heading">Attention!</h4>
              <p> This password is unrecoverable. If you lose it, you will lose access to your wallet and your coins. Write your password on paper and save it.</p>
            </MDBAlert>
          </div>
          <h4>
            <MDBBadge color="warning">warning</MDBBadge>
            <p>asdfasd asdfasdfas dfasdf asdfasd f</p>
          </h4>

          <MDBInput type="checkbox" />
        </div>
      </MDBContainer>
    );
  }
}

export default Debug;