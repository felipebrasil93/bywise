import React, { Component } from 'react';
import WalletTools from '../domains/WalletTools';
import Network from '../domains/Network';
import { MDBContainer, MDBIcon, MDBBadge } from "mdbreact";

const Loading = () => {
    return (
        <div className="spinner-border text-primary" role="status">
            <span className="sr-only">Loading...</span>
        </div>
    )
}

const Balance = (props) => {
    return (
        <h1>
            <span>{props.value} HDR </span>
            <MDBBadge pill color="indigo" style={{cursor: 'pointer'}} onClick={props.update}>
                <MDBIcon icon="sync-alt"/>
            </MDBBadge>
        </h1>
    )
}

class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            balance: WalletTools.getBalance(),
            sync: false
        }
        this.update = this.update.bind(this);
    }

    componentWillUnmount() {
        if (this.createWallets !== null) {
            clearInterval(this.createWallets);
            this.createWallets = null;
        }
    }

    update() {
        this.setState({ sync: true });
        let wallets = WalletTools.getWallets();
        let data = [];
        wallets.forEach(w => {
            w.balance = 0;
            w.lastUse = 0;
            data = [...data, WalletTools.hexToBase64(w.publicKeyHash)];
        });
        Network.request('/wallets', data, 'post', (ws => {
            let total = '----';
            if (ws !== null) {
                total = 0;
                ws.forEach(balance => {
                    total += balance.balanceValue;
                    wallets.forEach(w => {
                        if (WalletTools.hexToBase64(w.publicKeyHash) === balance.publicKeyHash) {
                            w.balance = balance.balanceValue;
                            w.lastUse = balance.lastUse;
                        }
                    });
                });
                WalletTools.saveWallets(wallets);
                WalletTools.saveBalance(total);
            }
            this.setState({ balance: total, sync: false });
        }));
    }

    render() {
        return (
            <MDBContainer style={{ height: "80vh" }}>
                <div className="align-baseline text-center p-5">
                    {this.state.sync ? <Loading /> : <Balance value={this.state.balance} update={this.update} />}
                </div>
            </MDBContainer>
        );
    }
}
export default Home;