import React, { Component } from "react";
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import "./index.css";
import Home from './pages/Home';
import Nodes from './pages/Nodes';
import Wellcome from './pages/Wellcome';
import Send from './pages/Send';
import Receiver from './pages/Receiver';
import Debug from './pages/Debug';
import NavBar from './components/NavBar';
import WalletTools from "./domains/WalletTools";

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    WalletTools.haveWallets() ? (
      <Component {...props} />
    ) : (
        <Redirect to={{ pathname: '/wellcome', state: { from: props.location } }} />
      )
  )} />
);

class App extends Component {

  componentDidMount() {
    //Network.startConnectProcess();
  }

  render() {
    return (
      <Router>
        <div className="flyout">
          <NavBar />
          <Switch>
            <PrivateRoute path="/" exact component={Home} />
            <PrivateRoute path="/nodes" component={Nodes} />
            <PrivateRoute path="/send" component={Send} />
            <PrivateRoute path="/receiver" component={Receiver} />
            <Route path="/wellcome" component={Wellcome} />
            <Route path="/debug" exact component={Debug} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
