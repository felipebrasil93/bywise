import NodeRSA from 'node-rsa';
import bs58 from 'bs58';
import sha256 from 'js-sha256';
import bigDecimal from 'js-big-decimal';
const HASH_SIZE = 10;
const INIT_ADDRESS = '047344cc3c51';
const MAIN_NET_ADDR = '01';
//const TEST_NET_ADDR = '02';

const hexToBase64 = (str) => {
    return btoa(String.fromCharCode.apply(null,
        str.replace(/\r|\n/g, "").replace(/([\da-fA-F]{2}) ?/g, "0x$1 ").replace(/ +$/, "").split(" "))
    );
}

class Wallet {

    key = null;
    balance = null;
    published = null;

    constructor() {
        this.key = new NodeRSA({ b: 1024 });
        this.balance = new bigDecimal('0');
        this.published = false;
    }

    importKey(privateKeyHex){
        this.key.importKey(Buffer.from(privateKeyHex, 'hex'), 'pkcs8-private-der');
    }

    getPublicKeyHex() {
        return Buffer.from(this.key.exportKey('pkcs8-public-der')).toString('hex');
    }

    getPublicKey() {
        return hexToBase64(Buffer.from(this.key.exportKey('pkcs8-public-der')).toString('hex'));
    }

    getPublicKeyHash() {
        return sha256(this.key.exportKey('pkcs8-public-der'));
    }

    getPrivateKeyHex() {
        return Buffer.from(this.key.exportKey('pkcs8-private-der')).toString('hex');
    }

    getPrivateKey() {
        return hexToBase64(Buffer.from(this.key.exportKey('pkcs8-private-der')).toString('hex'));
    }

    getAddress() {
        let publicKeyHash = sha256(this.key.exportKey('pkcs8-public-der'));
        let preAddr = INIT_ADDRESS + MAIN_NET_ADDR + publicKeyHash;
        let hash = sha256(Buffer.from(preAddr, 'hex')).substring(0, HASH_SIZE * 2);
        let addr = preAddr + hash;
        return bs58.encode(Buffer.from(addr, 'hex'));
    }

    extractPublicKeyHash(addr) {
        addr = bs58.decode(addr).toString('hex');
        let preAddr = addr.substring(0, addr.length - HASH_SIZE * 2);
        let hash = addr.substring(addr.length - HASH_SIZE * 2, addr.length);
        if (hash === sha256(Buffer.from(preAddr, 'hex')).substring(0, HASH_SIZE * 2)) {
            let INIT = preAddr.substring(0, (INIT_ADDRESS + MAIN_NET_ADDR).length);
            if (INIT === INIT_ADDRESS + MAIN_NET_ADDR) {
                return hexToBase64(preAddr.substring(INIT.length, preAddr.length));
            }
        }
        return null;
    }
}
export default Wallet;