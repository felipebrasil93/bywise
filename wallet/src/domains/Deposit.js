import bigDecimal from 'js-big-decimal';
import sha512 from 'js-sha512';

class Deposit {

    creditedPublicKeyHash = null;
    balanceValue = null;
    feeValue = null;
    created = null;
    inputsSize = 0;
    inputs = [];

    constructor(creditedPublicKeyHash, balanceValue, feeValue) {
        this.creditedPublicKeyHash = creditedPublicKeyHash;
        this.balanceValue = balanceValue;
        this.feeValue = feeValue;
        this.created = new Date().getTime();
    }

    getPreHashDeposit() {
        return sha512(sha512(Buffer.from(
            this.base64ToHex(this.creditedPublicKeyHash) +
            this.asciiToHexa(this.balanceValue) +
            this.asciiToHexa(this.feeValue) +
            this.asciiToHexa(this.created.toString()), 'hex')));
    }

    addInput(wallet, txValue) {
        let preHash = sha512(sha512(Buffer.from(
            wallet.getPublicKeyHex() +
            this.getPreHashDeposit() +
            this.asciiToHexa(txValue) +
            this.asciiToHexa(this.inputs.length.toString()), 'hex')));
        let entire = {
            publicKey: wallet.getPublicKey(),
            txValue: txValue,
            position: this.inputs.length,
            signature: wallet.key.sign(preHash, 'base64', 'hex')
        }
        this.inputs = [...this.inputs, entire];
        this.inputsSize++;
    }

    hexToBase64 = (str) => {
        return btoa(String.fromCharCode.apply(null,
            str.replace(/\r|\n/g, "").replace(/([\da-fA-F]{2}) ?/g, "0x$1 ").replace(/ +$/, "").split(" "))
        );
    }

    base64ToHex = (str) => {
        for (var i = 0, bin = atob(str.replace(/[ \r\n]+$/, "")), hex = []; i < bin.length; ++i) {
            let tmp = bin.charCodeAt(i).toString(16);
            if (tmp.length === 1) tmp = "0" + tmp;
            hex[hex.length] = tmp;
        }
        return hex.join("");
    }

    asciiToHexa(str) {
        var arr1 = [];
        for (var n = 0, l = str.length; n < l; n++) {
            var hex = Number(str.charCodeAt(n)).toString(16);
            arr1.push(hex);
        }
        return arr1.join('');
    }

    valueToHex(value) {
        let precision = "0.0000000000000001";
        value = value.add(new bigDecimal(precision));
        let valueString = value.getValue();
        if (valueString.substring(0, 2) === '0.') {
            valueString = valueString.substring(1, valueString.length - 1);
        } else {
            valueString = valueString.substring(0, valueString.length - 1);
        }
        return this.asciiToHexa(valueString);
    }
}
export default Deposit;