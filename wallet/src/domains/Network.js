
class Network {

    static nodes = [{ host: "192.168.0.14", port: 5478, token: null }];
    static connectedNode = null;

    static startConnectProcess() {
        this.connectedNode = null;
        let node = this.nodes[Math.floor(Math.random() * this.nodes.length)];
        this.getInfo(node);
    }

    static request(resource, body, method, affter) {
        if (this.connectedNode == null) {
            this.startConnectProcess();
            if (affter != null) {
                affter(null);
            }
        } else {
            let data;
            if (method === 'get' && body != null) {
                data = {
                    method: method,
                    headers: { 'Content-Type': 'application/json', 'Authorization': this.connectedNode.token }
                }
            } else {
                data = {
                    method: method,
                    headers: { 'Content-Type': 'application/json', 'Authorization': this.connectedNode.token },
                    body: JSON.stringify(body)
                }
            }
            if (affter != null) {
                fetch("http://" + this.connectedNode.host + ":" + this.connectedNode.port + "/server/v1" + resource, data)
                    .then(r => r.json())
                    .then(r => {
                        affter(r);
                    })
                    .catch((ex) => {
                        affter(null);
                        console.log(ex);
                        this.startConnectProcess();
                    });
            } else {
                fetch("http://" + this.connectedNode.host + ":" + this.connectedNode.port + "/server/v1" + resource, data)
                    .catch((ex) => {
                        console.log(ex);
                        this.startConnectProcess();
                    });
            }
        }
    }

    static getRandomNode() {
        return this.nodes[Math.floor(Math.random() * this.nodes.length)];
    }

    static getInfo(node) {
        fetch("http://" + node.host + ":" + node.port + "/server/v1/nodes/info")
            .then(r => r.json())
            .then(json => {
                json.nodes.forEach(nodeDTO => {
                    let repeted = false;
                    this.nodes.forEach(node => {
                        if (nodeDTO.address === node.host && nodeDTO.port === node.port) {
                            repeted = true;
                        }
                    });
                    if (!repeted) {
                        this.nodes = [...this.nodes, { host: nodeDTO.address, port: nodeDTO.port, token: null }];
                    }
                });
                this.tryConnect(node);
            })
            .catch((ex) => {
                console.log(ex);
                alert("Failed to connect");
            });
    }

    static refreshToken() {
        console.log('refreshToken');
        if (this.connectedNode != null) {
            this.tryConnect(this.connectedNode);
        } else {
            this.startConnectProcess();
        }
    }

    static tryConnect(node) {
        fetch("http://" + node.host + ":" + node.port + "/server/v1/nodes/wallet/handshake",
            {
                method: 'post',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    network: "MAIN_NET",
                    type: "WALLET",
                    version: "1",
                    software: "intely.app",
                    timestamp: new Date().getTime()
                })
            }).then(r => r.json())
            .then(json => {
                if (json.sucess) {
                    node.token = json.token;
                    this.connectedNode = node;
                    this.connectedNode['name'] = json.name;
                    this.connectedNode['software'] = json.software;
                    this.connectedNode['block'] = json.height;
                    this.connectedNode['version'] = json.version;
                    console.log("Connected node " + node.host + ":" + node.port);
                } else {
                    console.log(json.message);
                }
            })
            .catch((ex) => {
                console.log(ex);
                alert("Failed to connect");
            });
    }
}

export default Network;