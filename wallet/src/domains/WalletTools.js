
class WalletTools {
    static WalletsFile = "wallets_data";
    static BalanceType = "balance";

    static saveBalance = (balance) => {
        localStorage.setItem(this.BalanceType, balance);
    }

    static getBalance = () => {
        return localStorage.getItem(this.BalanceType);
    }

    static haveWallets = () => {
        return localStorage.getItem(this.WalletsFile) != null;
    }
    
    static saveWallets = (wallets) => {
        localStorage.setItem(this.WalletsFile, JSON.stringify(wallets));
    }

    static getWallets = () => {
        return JSON.parse(localStorage.getItem(this.WalletsFile));
    }

    static hexToBase64 = (str) => {
        return btoa(String.fromCharCode.apply(null,
            str.replace(/\r|\n/g, "").replace(/([\da-fA-F]{2}) ?/g, "0x$1 ").replace(/ +$/, "").split(" "))
        );
    }

    static base64ToHex = (str) => {
        for (var i = 0, bin = atob(str.replace(/[ \r\n]+$/, "")), hex = []; i < bin.length; ++i) {
            let tmp = bin.charCodeAt(i).toString(16);
            if (tmp.length === 1) tmp = "0" + tmp;
            hex[hex.length] = tmp;
        }
        return hex.join("");
    }
}

export default WalletTools;
